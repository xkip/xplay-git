﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MyUtils;
using MyUtils.PInvoke;
using XPlay.Storage;
using Path = System.IO.Path;

namespace XPlay
{
	/// <summary>
	/// Interaction logic for EditTask.xaml
	/// </summary>
	public partial class EditTask
	{
		private readonly XDropBoxStorage _storage;

		public EditTask(XDropBoxStorage storage)
		{
			_storage = storage;
			InitializeComponent();
		}

		private void OK_Button_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void IconChooser_SetIcon(object sender, SetEventArgs e)
		{
			using (var wc = new WebClient())
			{
				var blob = wc.DownloadData(e.Info.Url);
				var name = Guid.NewGuid().ToString("N") + Path.GetExtension(e.Info.Url);
				_storage.BlobWrite("icon", name, new MemoryStream(blob));
				((XTaskViewModel)DataContext).Model.IconName = name;
			}
		}

		private void Title_TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			iconChooser.Finder.Query = Regex.Matches(((TextBox)sender).Text, "\\w{3,}").Cast<Match>().Select(x => x.Value).Join(" ");
		}

	}
}
