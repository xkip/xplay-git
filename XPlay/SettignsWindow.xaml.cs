﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using XPlay.Properties;

namespace XPlay
{
	/// <summary>
	/// Interaction logic for SettignsWindow.xaml
	/// </summary>
	public partial class SettignsWindow : Window
	{
		public SettignsWindow()
		{
			InitializeComponent();
		}

		private void OK_Button_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
			Settings.Default.Save();
			_allowClose = true;
			Close();
		}

		private bool _allowClose;

		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			base.OnClosing(e);
			e.Cancel = !_allowClose;
		}
	}
}
