﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Windows.Input;
using MyUtils.UI;
using XPlay.IconFinderApi;
using UserControl = System.Windows.Controls.UserControl;

namespace XPlay
{
	/// <summary>
	/// Interaction logic for IconChooser.xaml
	/// </summary>
	public partial class IconChooser : UserControl
	{
		readonly IconFinder _finder = new IconFinder {SizeMin = 0, SizeMax = 128};

		public IconChooser()
		{
			InitializeComponent();
			DataContext = Finder;
		}
		
		public ICommand Set
		{
			get { return new DelegateCommand(x=> OnSetIcon((IconInfo)x)); }
		}

		public IconFinder Finder
		{
			get { return _finder; }
		}

		public event EventHandler<SetEventArgs> SetIcon;

		protected void OnSetIcon(IconInfo e)
		{
			OnSetIcon(new SetEventArgs(e));
		}

		protected void OnSetIcon(SetEventArgs e)
		{
			var handler = SetIcon;
			if (handler != null) handler(this, e);
		}
	}

	public class SetEventArgs : EventArgs
	{
		private readonly IconInfo _info;

		public SetEventArgs(IconInfo info)
		{
			_info = info;
		}

		public IconInfo Info
		{
			get { return _info; }
		}
	}
}
