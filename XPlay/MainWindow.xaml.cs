﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using Microsoft.Win32;
using MyUtils.PInvoke;
using XPlay.Model;
using XPlay.Properties;
using XPlay.UI.Toolbar.Shell;


namespace XPlay
{
	public partial class MainWindow
	{
		public MainWindow()
		{
			_helper = new WindowInteropHelper(this);

			InitializeComponent();
			DataContext = _viewModel = new WindowViewModel();
			Loaded += MainWindowLoaded;
			Closed += MainWindowClosed;
			LocationChanged += MainWindowLocationChanged;
			SystemEvents.UserPreferenceChanged += SystemEventsUserPreferenceChanged;
			_timer = new Timer(1000);
			_timer.Elapsed += CallBack;
		}

		void CallBack(object state, ElapsedEventArgs elapsedEventArgs)
		{
			_timer.Enabled = false;
			Dispatcher.Invoke((Action)Apply);
		}

		private readonly Timer _timer;

		void SystemEventsUserPreferenceChanged(object sender, UserPreferenceChangedEventArgs e)
		{
			_timer.Enabled = false;
			_timer.Enabled = true;
		}

		void MainWindowLocationChanged(object sender, EventArgs e)
		{
			Left = 0;
		}

		readonly WindowInteropHelper _helper;
		readonly WindowViewModel _viewModel;

		void MainWindowClosed(object sender, EventArgs e)
		{
			_viewModel.Dispose();
			Application.Current.Shutdown();
		}

		void MainWindowLoaded(object sender, RoutedEventArgs e)
		{
			_toolbarTransparent.SizeChanged += ToolbarSizeChanged;
			_toolbarTransparent.LocationChanged += ToolbarLocationChanged;
			_toolbarTransparent.Activated += ToolbarActivated;
			_toolbarTransparent.Width = 64;
			_toolbarTransparent.WindowStyle = WindowStyle.None;
			_toolbarTransparent.Background = Brushes.Transparent;
			_toolbarTransparent.AllowsTransparency = true;
			_toolbarTransparent.Left = -10000;
			_toolbarTransparent.Top = -10000;

			_toolbarNormal.SizeChanged += ToolbarSizeChanged;
			_toolbarNormal.LocationChanged += ToolbarLocationChanged;
			_toolbarNormal.Activated += ToolbarActivated;
			_toolbarNormal.Width = 64 + (GlassWindow.GlassEnabled ? 6 : 0);
			_toolbarNormal.WindowStyle = WindowStyle.ToolWindow;
			_toolbarNormal.Left = -10000;
			_toolbarNormal.Top = -10000;
			_toolbarNormal.Loaded += _toolbarNormal_Loaded;
			_toolbarNormal.WidthExtra = GlassWindow.GlassEnabled ? -6 : 0;
			_toolbarNormal.HeightExtra = GlassWindow.GlassEnabled ? -6 : 0;

			Apply();

			DetectFolders();

			while (_dropBoxFolder == null || _storagePath == null || _privatePath == null)
			{
				new SettignsWindow().ShowDialog();
				DetectFolders();
			}

			_viewModel.InitStorage();
			
		}

		private string _dropBoxFolder;
		private string _storagePath;
		private string _privatePath;

		private void DetectFolders()
		{
			_dropBoxFolder = new[]
			                    	{
			                    		Settings.Default.DropBoxFolder,
			                    		@"%UserProfile%\Desktop\Dropbox",
			                    		@"%UserProfile%\Dropbox",
			                    	}.Select(Environment.ExpandEnvironmentVariables).FirstOrDefault(
										x => Directory.Exists(x) && File.ReadAllText(Path.Combine(x, "desktop.ini")).ToLowerInvariant().Contains("dropbox"));

			if (_dropBoxFolder != null)
			{
				if (Settings.Default.DropBoxFolder != _dropBoxFolder)
				{
					Settings.Default.DropBoxFolder = _dropBoxFolder;
					Settings.Default.Save();
				}
			}

			_storagePath = new[]
			                  	{
			                  		Settings.Default.StoragePath,
			                  		Path.Combine(_dropBoxFolder ?? Guid.NewGuid().ToString("N"), "XTasks"),
			                  		@"%UserProfile%\Desktop\Dropbox\XTasks",
			                  		@"%UserProfile%\Dropbox\XTasks",
			                  	}.Select(Environment.ExpandEnvironmentVariables).FirstOrDefault(Directory.Exists);

			if (_storagePath != null)
			{
				if (Settings.Default.StoragePath != _storagePath)
				{
					Settings.Default.StoragePath = _storagePath;
					Settings.Default.Save();
				}
			}


			_privatePath = new[]
			                  	{
			                  		Settings.Default.StoragePathPrivate,
			                  		Path.Combine(_dropBoxFolder ?? Guid.NewGuid().ToString("N"), "XTasksPrivate"),
			                  		@"%UserProfile%\Desktop\Dropbox\XTasksPrivate",
			                  		@"%UserProfile%\Dropbox\XTasksPrivate",
			                  	}.Select(Environment.ExpandEnvironmentVariables).FirstOrDefault(Directory.Exists);

			if (_privatePath == null)
			{
				if (_dropBoxFolder != null && _storagePath != null)
				{
					Directory.CreateDirectory(_privatePath = Path.Combine(_dropBoxFolder, "XTasksPrivate"));
				}
			}

			if (_privatePath != null)
			{
				if (Settings.Default.StoragePathPrivate != _privatePath)
				{
					Settings.Default.StoragePathPrivate = _privatePath;
					Settings.Default.Save();
				}
			}


		}

		void _toolbarNormal_Loaded(object sender, RoutedEventArgs e)
		{
			var hwnd = _toolbarNormal.Handle;
			SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
		}

		private const int GWL_STYLE = -16;
		private const int WS_SYSMENU = 0x80000;
		[DllImport("user32.dll", SetLastError = true)]
		private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
		[DllImport("user32.dll")]
		private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

		void ToolbarActivated(object sender, EventArgs e)
		{
			Activate();
		}

		public IntPtr Handle
		{
			get { return _helper.Handle; }
		}

		void ToolbarLocationChanged(object sender, EventArgs e)
		{
			Dispatcher.BeginInvoke((Action) delegate
			                                	{
			                                		Left = _toolbar == null ? 0 : _toolbar.Left;
			                                	});
		}

		void ToolbarSizeChanged(object sender, SizeChangedEventArgs e)
		{
			ToolbarLocationChanged(null, null);
		}

		private ApplicationDesktopToolbar _toolbar;
		private readonly ApplicationDesktopToolbar _toolbarTransparent = new ApplicationDesktopToolbar();
		private readonly ApplicationDesktopToolbar _toolbarNormal = new ApplicationDesktopToolbar();

		private void ButtonCloseClick(object sender, RoutedEventArgs e)
		{
			Close();
		}

		enum Toolbar
		{
			None,
			Transparent,
			Normal,
		}

		private Toolbar _currentToolbar;
		Toolbar CurrentToolbar
		{
			get { return _currentToolbar; }
			set
			{
				if (value != _currentToolbar)
				{
					_currentToolbar = value;
					if (_toolbar != null)
					{
						_toolbar.Edge = ApplicationDesktopToolbar.AppBarEdges.Float;
						_toolbar.Hide();
					}
					switch (value)
					{
						case Toolbar.None:
							_toolbar = null;
							return;
						case Toolbar.Transparent:
							_toolbar = _toolbarTransparent;
							break;
						case Toolbar.Normal:
							_toolbar = _toolbarNormal;
							break;
						default:
							throw new ArgumentOutOfRangeException("value");
					}
					_toolbar.Show();
					_toolbar.Edge = ApplicationDesktopToolbar.AppBarEdges.Left;
				}
			}
		}

		public int PanelTransparency
		{
			get { return PrivateSettingsReader.Settings.TransparencyMode; }
			set
			{
				PrivateSettingsReader.Settings.TransparencyMode = value;
				PrivateSettingsReader.Save();
				Apply();
			}
		}

		PrivateSettingsReader PrivateSettingsReader
		{
			get { return _viewModel.PrivateSettingsReader.Value; }
		}

		public int PanelDock
		{
			get { return PrivateSettingsReader.Settings.DockMode; }
			set
			{
				PrivateSettingsReader.Settings.DockMode = value;
				PrivateSettingsReader.Save();
				Apply();
			}
		}

		enum Transparency
		{
			Transparent,
			SemiAero,
		}

		enum Docking
		{
			Overlay,
			Docked,
			Autohide,
		}

		void Apply()
		{
			Apply((Transparency)PanelTransparency, (Docking)PanelDock);
		}

		void Apply(Transparency transparency, Docking docking)
		{
			var glass = GlassWindow.GlassEnabled;

			switch (docking)
			{
				case Docking.Overlay:
					CurrentToolbar = Toolbar.None;
					break;
				case Docking.Docked:
				case Docking.Autohide:
					switch (transparency)
					{
						case Transparency.Transparent:
							CurrentToolbar = Toolbar.Transparent;
							_toolbar.Background = Brushes.Transparent;
							break;
						case Transparency.SemiAero:
							if (glass)
							{
								CurrentToolbar = Toolbar.Normal;
								_toolbar.Glass();
							}
							else
							{
								CurrentToolbar = Toolbar.Transparent;
								_toolbar.Background = (Brush)Resources["semiTransparentBg"];
							}
							break;
						default:
							throw new ArgumentOutOfRangeException("transparency");
					}
					_toolbar.AppbarSetAutoHideBar(docking == Docking.Autohide);
					break;
				default:
					throw new ArgumentOutOfRangeException("docking");
			}

			var res = Application.Current.Resources;
			var textStyle = res["TextBlock"] ?? res[typeof(TextBlock)];
			res.Remove("TextBlock");
			res.Remove(typeof(TextBlock));
			res.Add(glass ? (object)typeof(TextBlock) : "TextBlock", textStyle);
		}

		public bool Filter
		{
			get
			{
				return _viewModel.Filter;
			}
			set
			{
				_viewModel.Filter = value;
			}
		}

		private void StackPanel_Loaded(object sender, RoutedEventArgs e)
		{
			((StackPanel)sender).DataContext = this;
		}
	}
}
