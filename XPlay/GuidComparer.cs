using System;
using System.Collections.Generic;

namespace XPlay
{
	class GuidComparer : IComparer<Guid>
	{
		public static GuidComparer Instance = new GuidComparer();

		public int Compare(Guid x, Guid y)
		{
			return string.CompareOrdinal(x.ToString("N"), y.ToString("N"));
		}
	}
}