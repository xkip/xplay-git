using Obtics.Collections;
using Obtics.Values;

using XPlay.Model;

namespace XPlay
{
	public class NegotiatorViewModel
	{
		readonly User _user;
		readonly XTaskViewModel _parentVm;

		public IValueProvider<NegotiatorBase> Negotiator { get; set; }

		public NegotiatorViewModel(User user, XTaskViewModel parentVm)
		{
			_user = user;
			_parentVm = parentVm;

			Negotiator = _parentVm.Model.NegotiationProperties.FirstOrDefault(x => x.Name == "NegotiatedComment").Coalesce(new NegotiationProperty()).Value.Negotiators.FirstOrDefault(x => x.Id == _user.Id);
			// _included = ExpressionObserver.Execute(() => _parentVm.Model.GetNegotiation().NegotiationProperties.FirstOrDefault(x => x.Name == "NegotiatedComment").Value.Negotiators.Any(x => x.Id == _user.Id).Value);
			_included = ExpressionObserver.Execute(() => _parentVm.Model.NegotiationProperties.FirstOrDefault(x => x.Name == "NegotiatedComment").Coalesce(new NegotiationProperty()).Value.Negotiators.Any(x => x.Id == _user.Id).Value)

				.ReturnPath(x => {
					var pro = parentVm.Model.NegotiationProperties.FirstOrDefault(p => p.Name == "NegotiatedComment").Value;
					if (pro == null)
					{
						parentVm.Model.NegotiationProperties.Add(pro = new NegotiationProperty {Name = "NegotiatedComment"});
					}
					var usr = pro.Negotiators.FirstOrDefault(u => u.Id == _user.Id).Value;
					if (usr != null && !x)
					{
						pro.Negotiators.Remove(usr);
						_parentVm.Model.Save();
					}
					else if (usr == null && x)
					{
						pro.Negotiators.Add(new Negotiator {Id = _user.Id});
						_parentVm.Model.Save();
					}
				});
		}

		readonly IValueProvider<bool> _included;
		public IValueProvider<bool> Included
		{
			get { return _included; }
		}

		public User User
		{
			get { return _user; }
		}
	}
}