using System;

using XPlay.Model;

namespace XPlay
{
	public class NullUser : User
	{
		public NullUser()
		{
			UserName = "<None>";
		}

#pragma warning disable 108,114
		public Guid? Id { get; set; }
#pragma warning restore 108,114
	}
}