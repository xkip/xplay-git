using System;

using Obtics.Collections;
using Obtics.Values;

using XPlay.Model;
using XPlay.Storage;

namespace XPlay
{
	public class HistoryEventViewModel
	{
		readonly XTaskViewModel _parentVm;
		readonly XDropBoxStorage _storage;
		readonly HistoryEvent _model;
		readonly PropertyHistoryEvent _modelPhe;

		public HistoryEventViewModel(XDropBoxStorage storage, HistoryEvent model)
		{
			_storage = storage;
			_model = model;
			_modelPhe = model as PropertyHistoryEvent;
			_author = _storage.Users.FirstOrDefault(x => x.Id == _model.Author);
		}

		public HistoryEvent Model
		{
			get { return _model; }
		}

		readonly IValueProvider<User> _author;
		public IValueProvider<User> Author
		{
			get { return _author; }
		}

		public object OldValue
		{
			get
			{
				return ExpressionObserver.Execute(() =>
					_modelPhe.OldValue == null
						? "<None>"
						: _modelPhe.OldValue is Guid
							? _storage.GetById((Guid)_modelPhe.OldValue) ?? "<UnknownId>"
							: _modelPhe.OldValue is Guid?
								? _storage.GetById(((Guid?)_modelPhe.OldValue).GetValueOrDefault()) ?? "<UnknownId>"
								: _modelPhe.OldValue
					);
			}
		}

		public object NewValue
		{
			get
			{
				return ExpressionObserver.Execute(() =>
					_modelPhe.NewValue == null
						? "<None>"
						: _modelPhe.NewValue is Guid
							? _storage.GetById((Guid)_modelPhe.NewValue) ?? "<UnknownId>"
							: _modelPhe.NewValue is Guid?
								? _storage.GetById(((Guid?)_modelPhe.NewValue).GetValueOrDefault()) ?? "<UnknownId>"
								: _modelPhe.NewValue
					);
			}
		}
	}
}