﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace XPlay
{
	public class NotNullableDataTimeConvertor : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(value is DateTime)
			{
				var dt = (DateTime)value;
				if (dt == default(DateTime))
				{
					return "Never";
				}
				return dt.ToString((string)parameter);
			}
			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
