using System;
using System.ComponentModel;
using System.Timers;

using Obtics.Values;

namespace XPlay
{
	class CurrentDateValueProvider : IValueProvider<DateTime>, INotifyPropertyChanged
	{
		public static CurrentDateValueProvider Instance = new CurrentDateValueProvider();

		readonly Timer _timer;

		public CurrentDateValueProvider()
		{
			_timer = new Timer(15000);
			_timer.Elapsed += TimerElapsed;
			_lastReportedValue = Value;
		}

		void TimerElapsed(object sender, ElapsedEventArgs e)
		{
			PoolAndReport();
		}

		public void PoolAndReport()
		{
			var c = Value;
			if (_lastReportedValue != c)
			{
				_lastReportedValue = c;
				OnPropertyChanged();
			}
		}

		object IValueProvider.Value
		{
			get { return Value; }
			set { Value = (DateTime)value; }
		}

		DateTime _lastReportedValue;

		public DateTime Value
		{
			get { return DateTime.Now.Date; }
			set { throw new NotSupportedException(); }
		}

		public bool IsReadOnly
		{
			get { return true; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged()
		{
			OnPropertyChanged(new PropertyChangedEventArgs("Value"));
		}

		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, e);
			}
		}
	}
}