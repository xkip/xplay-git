﻿using System;
using System.Collections.Generic;
using System.Text;

using Obtics.Collections;
// using OE = Obtics.Collections.ObservableEnumerable;
using XPlay.Model;
using XPlay.Storage;

using EO = Obtics.Values.ExpressionObserver;

namespace XPlay
{
	partial class WindowViewModel
	{
		IEnumerable<XTask> _tasksSort1;

		partial void Init()
		{
			Filter = Filter;
			AllTasks = Storage.Tasks.Where(x => EO.Execute(() =>
				(string.IsNullOrWhiteSpace(AllTasksQuery) || x.Title.Contains(AllTasksQuery)) &&
				(AllTasksQueryDone == null || AllTasksQueryDone == x.IsDone)
				)).Select(CachedVm);
		}

		bool? _allTasksQueryDone;
		public bool? AllTasksQueryDone
		{
			get { return _allTasksQueryDone; }
			set
			{
				if (_allTasksQueryDone != value)
				{
					_allTasksQueryDone = value;
					OnPropertyInvalidate("AllTasksQueryDone");
				}
			}
		}

		public IEnumerable<RecycledItem> RecycledItems
		{
			get { return Storage.GetRecycle(); }
		}

		bool _filter;
		public bool Filter
		{
			get { return _filter; }
			set
			{
				_filter = value;
				OnPropertyInvalidate("Filter");
				if (_filter)
				{
					_tasksSort1 = Storage.Tasks
						.Where(x => EO.Execute(() =>
							!x.IsDone &&
								(!x.StartMin.HasValue || CurrentDateValueProvider.Instance.Value >= x.StartMin) &&
									(x.AssignedTo == null || x.AssignedTo == Negotiatable.Me)
							))
						.OrderByDescending(x => EO.Execute(() => x.Priority))
						.ThenBy(x => _storage.GetObjectId(x) ?? default(Guid), GuidComparer.Instance)
						.Take(16);

					Tasks = _tasksSort1.Where(t => EO.Execute(() => t.Abc == (_tasksSort1.Select(x => x.Abc).Distinct().OrderBy(x => x).First().Value))).Select(CachedVm);
				}
				else
				{
					_tasksSort1 = null;
					Tasks = Storage.Tasks
						.Where(x => EO.Execute(() =>
							!x.IsDone &&
								(!x.StartMin.HasValue || CurrentDateValueProvider.Instance.Value >= x.StartMin) &&
									(x.AssignedTo == null || x.AssignedTo == Negotiatable.Me)
							))
						.OrderByDescending(x => EO.Execute(() => x.Priority))
						.ThenBy(x => _storage.GetObjectId(x) ?? default(Guid), GuidComparer.Instance)
						.Select(CachedVm)
						.Take(16);
				}
			}
		}
	}
}
