﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Obtics.Collections;
using Obtics.Values;

using XPlay.Model;

using EO = Obtics.Values.ExpressionObserver;

namespace XPlay
{
	public partial class XTaskViewModel
	{
		public IValueProvider<User> CreatedByObj
		{
			get { return _parentVm.Storage.Users.FirstOrDefault(x => EO.Execute(() => x.Id == Model.CreatedBy)); }
		}

	}
}
