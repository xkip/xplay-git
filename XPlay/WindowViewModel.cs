﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows.Input;

using MyUtils;
using MyUtils.UI;

using XPlay.Model;
using XPlay.Properties;
using XPlay.Storage;

namespace XPlay
{
	public partial class WindowViewModel : ObservableObject, IDisposable
	{
		public WindowViewModel()
		{
//			var tasks = new List<XTask>
//			{
//				new XTask {Title = "Хранить задачи персистентно"},
//				new XTask {Title = "-Test1"},
//				new XTask {Title = "Панель хзадач - оверлэй"},
//				new XTask {Title = "Панель хзадач - док"},
//				new XTask {Title = "Панель хзадач - док + хайд"},
//				new XTask {Title = "Панель хзадач - трансперент"},
//				new XTask {Title = "Панель хзадач - semi трансперент"},
//				new XTask {Title = "Панель хзадач - gray grad"},
//				new XTask {Title = "Панель хзадач - Win7"},
//				new XTask {Title = "Сообщить родне о приезде"},
//				new XTask {Title = "Купить микроволновку"},
//				new XTask {Title = "Сохранять задачи в DropBox"},
//				new XTask {Title = "Шарить задачи с женой синхронно"},
//				new XTask {Title = "Назначать исполнителя"},
//				new XTask {Title = "Назначать изображение из файла"},
//				new XTask {Title = "Назначать изображение из экрана"},
//				new XTask {Title = "Назначать изображение из iconfinder"},
//				new XTask {Title = "Назначать изображение из google images"},
//				new XTask {Title = "Экран - список задач"},
//				new XTask {Title = "Экран - календарь"},
//				new XTask {Title = "Панель - Time Tracking"},
//				new XTask {Title = "Порт на телефон"},
//				new XTask {Title = "Enjoy life more"},
//				new XTask {Title = "Прочесть Alan Lakein -htgcoytayl"},
//				new XTask {Title = "Ooze"},
//				new XTask {Title = "Закончить с BBS"},
//			};

		}

		public void InitStorage()
		{
			Storage.StoragePath = Environment.ExpandEnvironmentVariables(Settings.Default.StoragePath);
			Storage.LoadAll();
			var ncc = Storage.Tasks as INotifyCollectionChanged;
			if (ncc != null)
			{
				ncc.CollectionChanged += NccCollectionChanged;
			}
			Init();

			var id = PrivateSettingsReader.Value.Settings.MyUserId;
			Negotiatable.Me = id;
			PrivateSettingsReader.Value.Save();

			var user = Storage.GetById(id);
			if (user == null)
			{
				Storage.Add(new User {Id = id, UserName = Environment.UserName}, id);
			}
		}

		public Lazy<PrivateSettingsReader> PrivateSettingsReader = new Lazy<PrivateSettingsReader>(() => new PrivateSettingsReader(Settings.Default.StoragePathPrivate));


		partial void Init();

		void NccCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == NotifyCollectionChangedAction.Add)
			{
				var xtask = e.NewItems[0] as XTask;
				if (xtask != null)
				{
					var id = Storage.GetObjectId(xtask);
					if (id.HasValue && id.Value == _openNew)
					{
						CachedVm(xtask).Edit.Execute(null);
					}
				}
			}
		}

		XTaskViewModel CachedVm(XTask model)
		{
			return Cache<XTaskViewModel>.Get[model, () => new XTaskViewModel(this, model)];
		}

		private readonly XDropBoxStorage _storage = new XDropBoxStorage();

		IEnumerable<object> _tasks;

		public IEnumerable<object> Tasks
		{
			get { return _tasks; }
			set
			{
				_tasks = value.ThombstoneObservable();
				OnPropertyInvalidate("Tasks");
			}
		}

		IEnumerable<XTaskViewModel> _allTasks;
		public IEnumerable<XTaskViewModel> AllTasks
		{
			get { return _allTasks; }
			set
			{
				_allTasks = value;
				OnPropertyInvalidate("AllTasks");
			}
		}

		public ICommand Add
		{
			get
			{
				return new DelegateCommand(delegate
				{
					_openNew = Storage.Add(XTask.CreaateNew(_storage.Users.Single(x=>x.Id == Negotiatable.Me)));
				});
			}
		}

		//TaskListWindow _tlw;

		public ICommand AllTasksList
		{
			get
			{
				return new DelegateCommand(delegate {
					new TaskListWindow
					{
						DataContext = this
					}.Show();
				});
			}
		}

		string _allTasksQuery;
		public string AllTasksQuery
		{
			get { return _allTasksQuery; }
			set
			{
				if (_allTasksQuery != value)
				{
					_allTasksQuery = value;
					OnPropertyInvalidate("AllTasksQuery");
				}
			}
		}

		public XDropBoxStorage Storage
		{
			get { return _storage; }
		}

		private Guid _openNew;

		public void Remove(XTaskViewModel xTaskViewModel)
		{
			Storage.Remove(xTaskViewModel.Model);
		}

		public void Dispose()
		{
			Storage.Dispose();
		}
	}
}