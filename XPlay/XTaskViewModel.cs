﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Input;
using System.Xaml;

using MyUtils;
using MyUtils.UI;

using Obtics.Collections;
using Obtics.Values;
using XPlay.Model;

using ObservableObject = XPlay.ObservableObject;

namespace XPlay
{
	public partial class XTaskViewModel : ObservableObject
	{
		readonly WindowViewModel _parentVm;
		private readonly XTask _model;

		public XTaskViewModel(WindowViewModel parentVm, XTask model)
		{
			_parentVm = parentVm;
			_model = model;
			_model.PropertyChanged += ModelPropertyChanged;
			Negotiators = _parentVm.Storage.Users.Select(x => Cache<NegotiatorViewModel>.Get[x, () => new NegotiatorViewModel(x, this)]);
			NegotiationProperty = Model.NegotiationProperties.FirstOrDefault(x => x.Name == "NegotiatedComment");
			Assignees = new[] {new NullUser()}.Concat(_parentVm.Storage.Users);
			_historyEvents = Model.History.Select(x => Cache<HistoryEventViewModel>.Get[x, () => new HistoryEventViewModel(parentVm.Storage, x)]);
		}

		void ModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (string.IsNullOrEmpty(e.PropertyName) || e.PropertyName == "IconName")
			{
				OnPropertyInvalidate("IconFile");
			}
		}

		readonly IEnumerable<HistoryEventViewModel> _historyEvents;
		public IEnumerable<HistoryEventViewModel> HistoryEvents
		{
			get { return _historyEvents; }
		}

		public XTask Model
		{
			get { return _model; }
		}

		public ICommand Edit
		{
			get
			{
				return new DelegateCommand(delegate
				{
					new EditTask(_parentVm.Storage)
					{
						DataContext = this,
					}.Show();
				});
			}
		}

		public ICommand Remove
		{
			get
			{
				return new DelegateCommand(delegate
				{
					_parentVm.Remove(this);
				});
			}
		}

		public string IconFile
		{
			get
			{
				var fileName = _parentVm.Storage.BlobFullName("icon", Model.IconName);
				if (string.IsNullOrEmpty(Model.IconName) || !File.Exists(fileName))
				{
					return @"Resources\applications-engineering.png";
				}
				return fileName;
			}
		}

		public IEnumerable<NegotiatorViewModel> Negotiators { get; set; }

				public IValueProvider<NegotiationProperty> NegotiationProperty { get; set; }


				public IEnumerable<User> Assignees { get; set; }
	}
}
