﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using XPlay.Properties;

namespace XPlay
{
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
			base.OnStartup(e);
		}

		void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			MessageBox.Show(e.ExceptionObject.ToString());
		}
	}
}
