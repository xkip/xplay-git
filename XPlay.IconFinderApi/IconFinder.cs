﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Threading;
using System.Xml;

using XPlay.Model;

namespace XPlay.IconFinderApi
{
	public enum IconFinderLicense
	{
		All,
		AllowCommercial,
		AllowCommercialNoLink,
	}
	public class IconInfo
	{
		public int Id { get; set; }
		public string Url { get; set; }
		public string Source { get;set; }
	}


	public class IconFinder : ObservableObject
	{
		private const string Key = "c83b596fc2e06ed3c22a39307faea8d5";

		public IconFinder()
		{
			_search = new DelayedExecutor(PerformQuery, 333, true);
			SizeMin = 16;
			SizeMax = 256;
			Query = "task";
		}

		string _query;
		public string Query
		{
			get { return _query; }
			set
			{
				if (_query != value)
				{
					_query = value;
					OnPropertyInvalidate("Query");
					_search.Execute();
				}
			}
		}

		readonly DelayedExecutor _search;

		public IconFinderLicense License { get; set; }
		public int SizeMin { get; set; }
		public int SizeMax { get; set; }

		public ObservableCollection<IconInfo> Icons
		{
			get { return _icons; }
		}

		private string _iconFinderXml;
		private string _googleImageApiJson;

		private readonly ObservableCollection<IconInfo> _icons = new ObservableCollection<IconInfo>();

		public void PerformQuery()
		{
			ThreadPool.QueueUserWorkItem(delegate {
				PerformQueryCore();
			});
		}

		public void PerformQueryNextPage()
		{

		}

		[DebuggerStepThrough]
		public void PerformQueryCore()
		{
			try
			{
				using (var cli = new WebClient())
				{
					_iconFinderXml = cli.DownloadString(string.Format("http://www.iconfinder.com/xml/search/?q={0}&c=32&p=0&min={1}&max={2}&api_key={3}", Uri.EscapeUriString(Query), SizeMin, SizeMax, Key));

					var doc = new XmlDocument();
					doc.LoadXml(_iconFinderXml.Replace("&", "&amp;"));
					//						var xrs = new XmlReaderSettings {CheckCharacters = false};
					//						using (var xr = XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(_xml)), xrs))
					//						{
					//							doc.Load(xr);
					//						}
					// var nsman = new XmlNamespaceManager(doc.NameTable);
					// var ns = "http://schemas.microsoft.com/developer/msbuild/2003";
					// nsman.AddNamespace("x", ns);
					Application.Current.Dispatcher.Invoke((Action)delegate
					{
						_icons.Clear();
						foreach (XmlNode icon in doc.SelectNodes("results/iconmatches/icon"))
						{
							_icons.Add(new IconInfo
							{
								Id = int.Parse(icon.SelectSingleNode("id").InnerText),
								Url = icon.SelectSingleNode("image").InnerText,
								Source = "",
							});
						}
					});

					_googleImageApiJson = cli.DownloadString(string.Format("http://ajax.googleapis.com/ajax/services/search/images?v=1.0&imgsz=small&safe=off&imgtype=clipart&as_filetype=png&rsz=8&q={0}", Uri.EscapeUriString(Query), SizeMin, SizeMax, Key)); // TODO DEPRECATED AT 2014

					var jss = new JavaScriptSerializer();
					var dict = jss.Deserialize<dynamic>(_googleImageApiJson);

					Application.Current.Dispatcher.Invoke((Action)delegate
					{
						foreach (dynamic item in dict["responseData"]["results"])
						{
							_icons.Add(new IconInfo
							{
								Source = "Google",
								Url = item["url"],
							});
						}
					});
				}
			}
			catch
			{
			}
		}
	}
}
