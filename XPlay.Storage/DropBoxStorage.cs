﻿using System;
using System.Collections;
using System.Collections.ObjectModel;

using MyUtils;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xaml;

using XPlay.Model;

namespace XPlay.Storage
{
	public class RecycledItem
	{
		public RecycledItem(string display, Guid id, DateTime lastWriteTimeUtc)
		{
			Display = display;
			Id = id;
			Modified = lastWriteTimeUtc;
		}

		public string Display { get; private set; }
		public Guid Id { get; private set; }
		public DateTime Modified { get; private set; }

		public TimeSpan Elapsed
		{
			get { return DateTime.UtcNow - Modified; }
		}

		public TimeSpan Left
		{
			get { return DropBoxStorage.RecycleDays - Elapsed; }
		}

		public string LeftString
		{
			get { return Left.ToSpanString(); }
		}

		public override string ToString()
		{
			return Display;
		}
	}

	public class DropBoxStorage : IDisposable
	{
		private readonly ObservableCollection<object> _allObjects = new ObservableCollection<object>();

		public IEnumerable<object> AllObjects
		{
			get
			{
				return _allObjects;
			}
		}

		public DropBoxStorage()
		{
			_watcher.Filter = "*.*";
			_watcher.NotifyFilter = NotifyFilters.LastWrite;
			_watcher.IncludeSubdirectories = false;
			_watcher.InternalBufferSize = 64*124;
			_watcher.Created += WatcherEvent;
			_watcher.Changed += WatcherEvent;
			_thread = new Thread(Worker) {IsBackground = true, Name = "DropBoxStorage Event Worker"};
			_thread.Start();

			_persistObject = new DelayedExecutor(PersistObjectCore, 100, true);
		}

		public IEnumerable<string> BlobList(string path = null)
		{
			var sp = StoragePath;
			if (!sp.EndsWith(Path.DirectorySeparatorChar.ToString(CultureInfo.InvariantCulture)))
			{
				sp += Path.DirectorySeparatorChar;
			}
			return Directory.GetFiles(Path.Combine(StoragePath, path ?? ""), "*.*", SearchOption.AllDirectories).Select(x => x.Substring(sp.Length));
		}

		public string BlobFullName(string path, string name)
		{
			return Path.Combine(StoragePath, path ?? "", name??"");
		}

		public void BlobWrite(string path, string name, Stream stream)
		{
			var fileName = BlobFullName(path, name);
			var dir = Path.GetDirectoryName(fileName);
			Directory.CreateDirectory(dir);

			using (var file = File.OpenWrite(fileName))
			{
				var buf = new byte[stream.Length];
				stream.Read(buf, 0, buf.Length);
				file.Write(buf, 0, buf.Length);
			}
		}

		public Stream BlobRead(string path, string name)
		{
			return File.OpenRead(BlobFullName(path, name));
		}

		private readonly Thread _thread;
		private bool _isDisposing;
		void Worker()
		{
			while (!_isDisposing)
			{
				_event.WaitOne();
				if (_isDisposing)
				{
					return;
				}
				FileSystemEventArgs ev;
				while ((ev = Dequeue()) != null)
				{
					Handle(ev);
				}
			}
		}

		private void Handle(FileSystemEventArgs ev)
		{
			try
			{
				if (PendingRemove == ev.FullPath)
				{
					var excludes = File.Exists(PendingRemove)
									? File.ReadAllLines(PendingRemove).Select(x => new Guid(x)).ToArray()
									: Enumerable.Empty<Guid>().ToArray();
					foreach (var exclude in excludes)
					{
						object obj;
						if (_mapIdToObject.TryGetValue(exclude, out obj))
						{
							Remove(obj);
						}
					}
				}
				else
				{
					LoadObject(ev.FullPath);
				}
			}
			catch (Exception ex)
			{
				Trace.TraceError(ex.ToString());
			}
		}

		private FileSystemEventArgs Dequeue()
		{
			FileSystemEventArgs ev;
			lock (_events)
			{
				ev = _events.Count > 0 ? _events.Dequeue() : null;
			}
			return ev;
		}

		void WatcherEvent(object sender, FileSystemEventArgs e)
		{
			lock (_events)
			{
				_events.Enqueue(e);
			}
			_event.Set();
		}

		private readonly Dictionary<Guid, object> _mapIdToObject = new Dictionary<Guid, object>();
		private readonly Dictionary<object, Guid> _mapObjectToId = new Dictionary<object, Guid>();

		public object this[Guid id]
		{
			get
			{
				object val;
				_mapIdToObject.TryGetValue(id, out val);
				return val;
			}
			private set
			{
				_mapIdToObject[id] = value;
				_mapObjectToId[value] = id;
			}
		}

		// notify
		protected virtual void OnNewLoaded(object obj)
		{
			DispatchableObservable.DefaultDispatcherInvoke(delegate
			{
				_allObjects.Add(obj);
			});
			ReSubscribe(obj);
		}

		// notify
		protected virtual void OnRemoving(object obj)
		{
			DispatchableObservable.DefaultDispatcherInvoke(delegate
			{
				_allObjects.Remove(obj);
			});
			Unsubscribe(obj);
		}

		void Unsubscribe(object obj)
		{
			var npc = obj as INotifyPropertyChanged;
			if (npc != null)
			{
				npc.PropertyChanged -= Pc;
			}
		}

		void Subscribe(object obj)
		{
			var npc = obj as INotifyPropertyChanged;
			if (npc != null && _mapObjectToId.ContainsKey(obj))
			{
				Debug.Assert(obj.GetType().Name == "XTask" || obj.GetType().Name == "User");
				npc.PropertyChanged += Pc;
			}
		}

		void ReSubscribe(object obj)
		{
			Debug.Assert(obj.GetType().Name == "XTask" || obj.GetType().Name == "User");
			var npc = obj as INotifyPropertyChanged;
			if (npc != null)
			{
				npc.PropertyChanged -= Pc;
				npc.PropertyChanged += Pc;
			}
		}

		void Pc(object sender, PropertyChangedEventArgs e)
		{
			if (!string.IsNullOrEmpty(e.PropertyName))
			{
				Trace.WriteLine("Property changed: Save object: " + e.PropertyName);
				PersistObject(_mapObjectToId[sender], sender);
			}
		}

		private readonly Queue<FileSystemEventArgs> _events = new Queue<FileSystemEventArgs>();
		private readonly AutoResetEvent _event = new AutoResetEvent(false);
		private readonly FileSystemWatcher _watcher = new FileSystemWatcher();

		public string StoragePath { get; set; }

		public IEnumerable<RecycledItem> GetRecycle()
		{
			var result = new List<RecycledItem>();
			TryFileV(delegate {
				var files = new DirectoryInfo(StoragePath).GetFiles();
				foreach (var recycledId in File.Exists(PendingRemove) ? File.ReadAllLines(PendingRemove) : new string[0])
				{
					var file = files.FirstOrDefault(x => string.Equals(x.Name, recycledId, StringComparison.InvariantCultureIgnoreCase));
					if (file != null)
					{
						try
						{
							result.Add(new RecycledItem(XamlServices.Load(file.FullName).ToString(), new Guid(recycledId), file.LastWriteTimeUtc));
						}
						catch (Exception ex)
						{
							XTrace.XTrace.Exception(ex);
						}
					}
				}
			});
			return result;
		}

		public void LoadAll()
		{
			_watcher.Path = StoragePath;
			_watcher.EnableRaisingEvents = true;

			var excludeLinesOriginal = File.Exists(PendingRemove) ? File.ReadAllLines(PendingRemove) : new string[0];
			var files = new DirectoryInfo(StoragePath).GetFiles();

			var excludeLines = excludeLinesOriginal.Distinct().Where(x => files.Any(y => y.Name == x)).ToArray();
			if (excludeLines.Length != excludeLinesOriginal.Length)
			{
				TryFileV(() => File.WriteAllLines(PendingRemove, excludeLines));
			}

			var excludes = excludeLines.Select(x => new Guid(x)).ToDictionary(x => x);

			foreach (var file in files)
			{
				Guid id;
				Guid.TryParse(file.Name, out id);
				if (id != default(Guid))
				{
					if (!excludes.ContainsKey(id))
					{
						LoadObject(file.FullName, id);
					}
					else
					{
						if (DateTime.UtcNow - file.LastWriteTimeUtc > RecycleDays)
						{
							TryFileV(() => file.Delete());
						}
					}
				}
			}
		}

		string PendingRemove
		{
			get { return Path.Combine(StoragePath, "_PendingRemove.lst"); }
		}

		public void MarkForRemove(Guid id)
		{
			TryFileV(id, i => {
				if (!File.ReadAllLines(PendingRemove).Any(x => x == i.ToString("N")))
				{
					File.AppendAllText(PendingRemove, i.ToString("N") + Environment.NewLine);
				}
			});
		}

		private void LoadObject(string name, Guid id = default(Guid))
		{
			try
			{
				if (id == default(Guid))
				{
					Guid.TryParse(Path.GetFileName(name), out id);
				}

				if (id != default(Guid))
				{
					var obj = TryRead(name);
					if (obj != null)
					{
						object known;
						_mapIdToObject.TryGetValue(id, out known);
						if (known == null)
						{
							this[id] = obj;
							OnNewLoaded(obj);
						}
						else
						{
							DispatchableObservable.DefaultDispatcherInvoke(delegate
							{
								UpdateFromTo(obj, known);
							});
						}
					}
				}
			}
			catch (Exception ex)
			{
				XTrace.XTrace.Exception(ex);
			}
		}

		[DebuggerStepThrough]
		private static void TryFileV(Action act)
		{
			TryFile<object, object>(null, _ =>
			{
				act();
				return null;
			});
		}

		[DebuggerStepThrough]
		private static void TryFileV<T>(T arg, Action<T> act)
		{
			TryFile<T, object>(arg, x =>
			{
				act(x);
				return null;
			});
		}

		[DebuggerStepThrough]
		private static TR TryFile<T, TR>(T arg, Func<T, TR> act)
		{
			TR obj = default(TR);
			for (int i = 0; i < 5; i++)
			{
				try
				{
					lock (_fsLock)
					{
						obj = act(arg);
					}
					break;
				}
				catch (IOException)
				{
					Thread.Sleep(300);
				}
			}
			return obj;
		}

		static readonly object _fsLock = new object();

		[DebuggerStepThrough]
		private static object TryRead(string name)
		{
			var obj = TryFile(name, XamlServices.Load);
			var neg = obj as Negotiatable;
			if (neg != null)
			{
				neg.EnableChangeTracking();
			}
			return obj;
		}

		private void UpdateFromTo(object from, object to)
		{
			if (from == null) throw new ArgumentNullException("from");
			if (to == null) throw new ArgumentNullException("to");

			var toN = to as Negotiatable;
			if (toN != null)
			{
				lock (toN.GetChangeTrackingSync())
				{
					toN.EnableChangeTracking(false);
					try
					{
						UpdateFromToCore(from, to);
					}
					finally
					{
						toN.EnableChangeTracking(true);
					}
				}
			}
			else
			{
				UpdateFromToCore(from, to);
			}
		}

		void UpdateFromToCore(object @from, object to)
		{
			var type = @from.GetType();
			if (to.GetType() != type)
			{
				throw new Exception("Can not update objects of different type");
			}

			foreach (var pro in type.GetProperties())
			{
				var val = pro.GetValue(@from, null);
				if (pro.CanRead && pro.CanWrite && pro.Attribute<DesignerSerializationVisibilityAttribute>() == null) // simple
				{
					Unsubscribe(to);
					pro.SetValue(to, val, null);
					Subscribe(to);
				}
				else if (pro.CanRead && typeof(IEnumerable<object>).IsAssignableFrom(pro.PropertyType)) // list
				{
					var source = new HashSet<object>(((IEnumerable<object>)val));
					var targetCollection = (IList)pro.GetValue(to, null);
					var target = new HashSet<object>(targetCollection.Cast<object>());

					var sourceNew = new HashSet<object>(source);
					sourceNew.ExceptWith(target);

					var targetOld = new HashSet<object>(target);
					targetOld.ExceptWith(source);

					// remove old
					foreach (var old in targetOld)
					{
						targetCollection.Remove(old);
					}
					// add new
					foreach (var newItem in sourceNew)
					{
						targetCollection.Add(newItem);
					}
					// update existing
					foreach (var item in source)
					{
						var targetItem = targetCollection.Cast<object>().FirstOrDefault(x => x.Equals(item));
						if (targetItem != null)
						{
							UpdateFromTo(item, targetItem);
						}
						else
						{
							Debugger.Break();
						}
					}
				}
//				else if (pro.CanRead && typeof(IList).IsAssignableFrom(pro.PropertyType)) // composite
//				{
//					var val2 = pro.GetValue(to, null);
//					UpdateFromTo(val, val2);
//				}
			}

			var oo = to as ObservableObject;
			if (oo != null)
			{
				oo.OnPropertyInvalidate();
			}
		}

		public void Dispose()
		{
			_isDisposing = true;
			_watcher.Created -= WatcherEvent;
			_watcher.Changed -= WatcherEvent;
			_watcher.Dispose();
			_event.Set();
		}

		public Guid Add(object value, Guid? idSuggest = null)
		{
			Guid id;
			if (!_mapObjectToId.TryGetValue(value, out id))
			{
				id = idSuggest ?? Guid.NewGuid();
			}
			PersistObject(id, value);
			return id;
		}

		public Guid? GetObjectId(object obj)
		{
			Guid id;
			if (_mapObjectToId.TryGetValue(obj, out id))
			{
				return id;
			}
			return null;
		}

		public object GetById(Guid id)
		{
			object obj;
			_mapIdToObject.TryGetValue(id, out obj);
			return obj;
		}

		private void PersistObject(Guid id, object value)
		{
			lock (_queue)
			{
				if (_queueSet.Add(id))
				{
					_queue.Enqueue(Tuple.Create(id, value));
				}
			}
			_persistObject.Execute();
		}

		readonly Queue<Tuple<Guid, object>> _queue = new Queue<Tuple<Guid, object>>();
		readonly HashSet<Guid> _queueSet = new HashSet<Guid>();

		private void PersistObjectCore()
		{
			Tuple<Guid, object> item = null;
			do
			{
				lock (_queue)
				{
					item = _queue.Count > 0 ? _queue.Dequeue() : null;
					if (item != null)
					{
						_queueSet.Remove(item.Item1);
					}
				}
				if (item != null)
				{
					// TODO check for tag and merge
					Trace.WriteLine("Save object...");
					TryFileV(() => XamlServices.Save(Path.Combine(StoragePath, item.Item1.ToString("N")), item.Item2));
				}
			} while (item != null);
		}

		readonly DelayedExecutor _persistObject;
		public static readonly TimeSpan RecycleDays = TimeSpan.FromDays(2);

		public void Remove(object model)
		{
			MarkForRemove(_mapObjectToId[model]);
			OnRemoving(model);
		}
	}
}
