using System.Collections.Generic;
using System.Collections.ObjectModel;

using MyUtils.UI;

using XPlay.Model;

namespace XPlay.Storage
{
	public class XDropBoxStorage : DropBoxStorage
	{
		private readonly ObservableCollection<XTask> _tasks = new ObservableCollection<XTask>();

		public IEnumerable<XTask> Tasks
		{
			get
			{
				return _tasks;
			}
		}

		private readonly ObservableCollection<User> _users = new ObservableCollection<User>();

		public IEnumerable<User> Users
		{
			get
			{
				return _users;
			}
		}

		protected override void OnNewLoaded(object obj)
		{
			base.OnNewLoaded(obj);
			var task = obj as XTask;
			if (task != null)
			{
				DispatchableObservable.DefaultDispatcherInvoke(delegate
				{
					_tasks.Add(task);
				});
			}
			var user = obj as User;
			if (user != null)
			{
				DispatchableObservable.DefaultDispatcherInvoke(delegate
				{
					_users.Add(user);
				});
			}
		}

		protected override void OnRemoving(object obj)
		{
			base.OnRemoving(obj);
			var task = obj as XTask;
			if (task != null)
			{
				DispatchableObservable.DefaultDispatcherInvoke(delegate
				{
					_tasks.Remove(task);
				});
			}
			var user = obj as User;
			if (user != null)
			{
				DispatchableObservable.DefaultDispatcherInvoke(delegate
				{
					_users.Remove(user);
				});
			}
		}
	}
}