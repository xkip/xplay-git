﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace ConsoleApplication1
{
	class Program
	{
		static void Main()
		{
			var cli = new WebClient();
			var data = cli.DownloadString("http://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=fuzzy%20monkey");

			var jss = new JavaScriptSerializer();
			var dict = jss.Deserialize<dynamic>(data);


			Console.WriteLine(data);

			foreach (dynamic item in dict["responseData"]["results"])
			{
				Console.WriteLine(item["url"]);
			}
			//Console.WriteLine(dict["responseData"]["results"]);
			//Console.WriteLine(dict.responseData.results);
		}
	}
}
