using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Media;
using MyUtils.PInvoke;

namespace XPlay.UI.Toolbar.Shell
{

	public class ApplicationDesktopToolbar : Window
	{
		// TODO multiple monitors
		// TODO user settigns cache
		readonly Lazy<System.Drawing.Size> _primaryMonitorSize = new Lazy<System.Drawing.Size>(() => SystemInformation.PrimaryMonitorSize);

		#region Enums
		enum AppBarMessages
		{
			/// <summary>
			/// Registers a new appbar and specifies the message identifier
			/// that the system should use to send notification messages to 
			/// the appbar. 
			/// </summary>
			New					= 0x00000000,	
			/// <summary>
			/// Unregisters an appbar, removing the bar from the system's 
			/// internal list.
			/// </summary>
			Remove				= 0x00000001,
			/// <summary>
			/// Requests a size and screen position for an appbar.
			/// </summary>
			QueryPos			= 0x00000002,	
			/// <summary>
			/// Sets the size and screen position of an appbar. 
			/// </summary>
			SetPos				= 0x00000003,	
			/// <summary>
			/// Retrieves the autohide and always-on-top states of the 
			/// Microsoft� Windows� taskbar. 
			/// </summary>
			GetState			= 0x00000004,	
			/// <summary>
			/// Retrieves the bounding rectangle of the Windows taskbar. 
			/// </summary>
			GetTaskBarPos		= 0x00000005,	
			/// <summary>
			/// Notifies the system that an appbar has been activated. 
			/// </summary>
			Activate			= 0x00000006,	
			/// <summary>
			/// Retrieves the handle to the autohide appbar associated with
			/// a particular edge of the screen. 
			/// </summary>
			GetAutoHideBar		= 0x00000007,	
			/// <summary>
			/// Registers or unregisters an autohide appbar for an edge of 
			/// the screen. 
			/// </summary>
			SetAutoHideBar		= 0x00000008,	
			/// <summary>
			/// Notifies the system when an appbar's position has changed. 
			/// </summary>
			WindowPosChanged	= 0x00000009,	
			/// <summary>
			/// Sets the state of the appbar's autohide and always-on-top 
			/// attributes.
			/// </summary>
			SetState			= 0x0000000a	
		}
		

		enum AppBarNotifications
		{
			/// <summary>
			/// Notifies an appbar that the taskbar's autohide or 
			/// always-on-top state has changed�that is, the user has selected 
			/// or cleared the "Always on top" or "Auto hide" check box on the
			/// taskbar's property sheet. 
			/// </summary>
			StateChange			= 0x00000000,	
			/// <summary>
			/// Notifies an appbar when an event has occurred that may affect 
			/// the appbar's size and position. Events include changes in the
			/// taskbar's size, position, and visibility state, as well as the
			/// addition, removal, or resizing of another appbar on the same 
			/// side of the screen.
			/// </summary>
			PosChanged			= 0x00000001,	
			/// <summary>
			/// Notifies an appbar when a full-screen application is opening or
			/// closing. This notification is sent in the form of an 
			/// application-defined message that is set by the ABM_NEW message. 
			/// </summary>
			FullScreenApp		= 0x00000002,	
			/// <summary>
			/// Notifies an appbar that the user has selected the Cascade, 
			/// Tile Horizontally, or Tile Vertically command from the 
			/// taskbar's shortcut menu.
			/// </summary>
			WindowArrange		= 0x00000003	
		}
		
		[Flags]
		public enum AppBarStates
		{
			AutoHide			= 0x00000001,
			AlwaysOnTop			= 0x00000002
		}


		public enum AppBarEdges
		{
			Left				= 0,
			Top					= 1,
			Right				= 2,
			Bottom				= 3,
			Float				= 4
		}
	
		// Window Messages		
		enum WM
		{
			ACTIVATE				= 0x0006,
			WINDOWPOSCHANGED		= 0x0047,
			NCHITTEST				= 0x0084
		}

		enum MousePositionCodes
		{
			HTERROR             = (-2),
			HTTRANSPARENT       = (-1),
			HTNOWHERE           = 0,
			HTCLIENT            = 1,
			HTCAPTION           = 2,
			HTSYSMENU           = 3,
			HTGROWBOX           = 4,
			HTSIZE              = HTGROWBOX,
			HTMENU              = 5,
			HTHSCROLL           = 6,
			HTVSCROLL           = 7,
			HTMINBUTTON         = 8,
			HTMAXBUTTON         = 9,
			HTLEFT              = 10,
			HTRIGHT             = 11,
			HTTOP               = 12,
			HTTOPLEFT           = 13,
			HTTOPRIGHT          = 14,
			HTBOTTOM            = 15,
			HTBOTTOMLEFT        = 16,
			HTBOTTOMRIGHT       = 17,
			HTBORDER            = 18,
			HTREDUCE            = HTMINBUTTON,
			HTZOOM              = HTMAXBUTTON,
			HTSIZEFIRST         = HTLEFT,
			HTSIZELAST          = HTBOTTOMRIGHT,
			HTOBJECT            = 19,
			HTCLOSE             = 20,
			HTHELP              = 21
		}

		#endregion Enums
        
		#region AppBar Functions



		private Boolean AppbarNew()
		{
			if (_callbackMessageId == 0)
				throw new Exception("CallbackMessageID is 0");

			if (_isAppbarMode)
				return true;

			_prevSize = new Size(Width, Height);
			_prevLocation = new Point(Left, Top);

			// prepare data structure of message
			var msgData = new ShellApi.APPBARDATA();
			msgData.cbSize = (UInt32)Marshal.SizeOf(msgData);
			msgData.hWnd = new WindowInteropHelper(this).Handle;
			msgData.uCallbackMessage = _callbackMessageId;

			// install new appbar
			var retVal = ShellApi.SHAppBarMessage((UInt32)AppBarMessages.New,ref msgData);
			_isAppbarMode = (retVal != IntPtr.Zero);
			
			SizeAppBar();
			
			return _isAppbarMode;
		}

		private void AppbarRemove()
		{
			_isAppbarMode = false;

//			Width = _prevSize.Width;
//			Height = _prevSize.Height;
//
//			Left = _prevLocation.X;
//			Top = _prevLocation.Y;

			AppbarRemove(_helper.Handle);
		}

//		public static void RemoveTaskBar()
//		{
//			AppbarRemove(TaskbarHandle);
//		}
//
//		public static void AddTaskBar()
//		{
//			//Appbar(TaskbarHandle);
//		}

//		[DllImport("user32.dll")]
//		static extern IntPtr FindWindow(string className, string windowText);
//
//		public static IntPtr TaskbarHandle
//		{
//			get { return FindWindow("Shell_TrayWnd", ""); }
//		}

		static ShellApi.APPBARDATA Appbar(IntPtr handle, AppBarMessages message, AppBarEdges? edge = null, ShellApi.RECT? rc = null, int? lParam = null)
		{
			var msgData = new ShellApi.APPBARDATA();
			msgData.cbSize = (UInt32)Marshal.SizeOf(msgData);
			msgData.hWnd = handle;

			if (edge.HasValue)
			{
				msgData.uEdge = (uint)edge.Value;
			}

			if (rc.HasValue)
			{
				msgData.rc = rc.Value;
			}

			if (lParam.HasValue)
			{
				msgData.lParam = lParam.Value;
			}

			ShellApi.SHAppBarMessage((UInt32)message, ref msgData);

			return msgData;
		}

		private static void AppbarRemove(IntPtr handle)
		{
			Appbar(handle, AppBarMessages.Remove);
		}

		private void AppbarQueryPos(ref ShellApi.RECT appRect)
		{
			AppbarQueryPos(Handle, Edge, ref appRect);
		}

		public static void AppbarQueryPos(IntPtr handle, AppBarEdges edge, ref ShellApi.RECT appRect)
		{
			appRect = Appbar(handle, AppBarMessages.QueryPos, edge, appRect).rc;
		}

		public IntPtr Handle
		{
			get { return _helper.Handle; }
		}

		public void AppbarSetPos(ref ShellApi.RECT appRect)
		{
			AppbarSetPos(Handle, Edge, ref appRect);
		}

		public static void AppbarSetPos(IntPtr handle, AppBarEdges edge, ref ShellApi.RECT appRect)
		{
			appRect = Appbar(handle, AppBarMessages.SetPos, edge, appRect).rc;
		}

		void AppbarActivate()
		{
			// prepare data structure of message
			var msgData = new ShellApi.APPBARDATA();
			msgData.cbSize = (UInt32)Marshal.SizeOf(msgData);
			msgData.hWnd = Handle;
			
			// send activate to the system
			ShellApi.SHAppBarMessage((UInt32)AppBarMessages.Activate, ref msgData);
		}

		public void AppbarWindowPosChanged()
		{
			Appbar(Handle, AppBarMessages.WindowPosChanged);
		}

		public static void AppbarWindowPosChanged(IntPtr handle)
		{
			Appbar(handle, AppBarMessages.WindowPosChanged);
		}

		public void AppbarSetAutoHideBar(Boolean hideValue)
		{
			AppbarSetAutoHideBar(Handle, Edge, hideValue);
		}

		public static void AppbarSetAutoHideBar(IntPtr handle, AppBarEdges edge, Boolean hideValue)
		{
			Appbar(handle, AppBarMessages.SetAutoHideBar, edge, lParam: (hideValue) ? 1 : 0);
			Appbar(handle, AppBarMessages.SetState, edge, lParam: (int)((hideValue) ? AppBarStates.AutoHide | AppBarStates.AlwaysOnTop : AppBarStates.AlwaysOnTop));
		}

		public ShellApi.RECT AppbarGetTaskbarPos()
		{
			return AppbarGetTaskbarPos(Handle);
		}

		public static ShellApi.RECT AppbarGetTaskbarPos(IntPtr handle)
		{
			return Appbar(handle, AppBarMessages.GetTaskBarPos).rc;
		}

		#endregion AppBar Functions
		
		#region Private Variables
		
		// saves the current edge
		private AppBarEdges _edge = AppBarEdges.Float;

		// saves the callback message id
		private readonly UInt32 _callbackMessageId;

		// are we in dock mode?
		private Boolean _isAppbarMode;

		// save the floating size and location
		private Size _prevSize;
		private Point _prevLocation;
		
		#endregion Private Variables

		HwndSource HwndSource;

		public ApplicationDesktopToolbar()
		{
			_sizeAppBarDelayed = new DelayedExecutor(() => Dispatcher.BeginInvoke((Action)SizeAppBar_));

			_helper = new WindowInteropHelper(this);
			ResizeMode = ResizeMode.NoResize;
			ShowInTaskbar = false;
			Topmost = true;

			// Register a unique message as our callback message
			_callbackMessageId = RegisterCallbackMessage();
			if (_callbackMessageId == 0)
			{
				throw new Exception("RegisterCallbackMessage failed");
			}

			SizeChanged += ApplicationDesktopToolbarSizeChanged;
			LocationChanged += ApplicationDesktopToolbar_LocationChanged;
			Loaded += ApplicationDesktopToolbarLoaded;
		}

		void ApplicationDesktopToolbar_LocationChanged(object sender, EventArgs e)
		{
			SizeAppBar();
		}

		void ApplicationDesktopToolbarLoaded(object sender, RoutedEventArgs e)
		{
			HwndSource = HwndSource.FromHwnd(Handle);
			HwndSource.AddHook(WndProc);

			ToolInit();
			_prevSize = Size;
			_prevLocation = Location;

		}

		public virtual void ToolInit()
		{
			// Width = 64;
			Edge = AppBarEdges.Left;
		}

		readonly WindowInteropHelper _helper;
			
		private static UInt32 RegisterCallbackMessage()
		{
			var uniqueMessageString = Guid.NewGuid().ToString();
			return ShellApi.RegisterWindowMessage(uniqueMessageString);
		}

		private readonly DelayedExecutor _sizeAppBarDelayed;

		void SizeAppBar()
		{
			_sizeAppBarDelayed.Execute();
		}

		protected void SizeAppBar_()
		{
			if (_edge != AppBarEdges.Float)
			{
				var rt = new ShellApi.RECT();

				if ((_edge == AppBarEdges.Left) || (_edge == AppBarEdges.Right))
				{
					rt.top = 0;
					rt.bottom = _primaryMonitorSize.Value.Height;
					if (_edge == AppBarEdges.Left)
					{
						rt.right = (int) _prevSize.Width;
					}
					else
					{
						rt.right = _primaryMonitorSize.Value.Width;
						rt.left = (int) (rt.right - _prevSize.Width);
					}
				}
				else
				{
					rt.left = 0;
					rt.right = _primaryMonitorSize.Value.Width;
					if (_edge == AppBarEdges.Top)
					{
						rt.bottom = (int) _prevSize.Height;
					}
					else
					{
						rt.bottom = _primaryMonitorSize.Value.Height;
						rt.top = (int) (rt.bottom - _prevSize.Height);
					}
				}

				AppbarQueryPos(ref rt);
				var rtOrigin = rt;

				switch (_edge)
				{
					case AppBarEdges.Left:
						rt.right = (int) (rt.left + _prevSize.Width);
						break;
					case AppBarEdges.Right:
						rt.left = (int) (rt.right - _prevSize.Width);
						break;
					case AppBarEdges.Top:
						rt.bottom = (int) (rt.top + _prevSize.Height);
						break;
					case AppBarEdges.Bottom:
						rt.top = (int) (rt.bottom - _prevSize.Height);
						break;
				}

				AppbarSetPos(ref rt);

				//rt = rtOrigin;
				Location = new Point(rt.left, rt.top);
				Size = new Size(Math.Max(0, rt.right - rt.left + WidthExtra), Math.Max(0, rt.bottom - rt.top + HeightExtra));
			}
		}

		#region WinProcMessage Handlers

		void OnAppbarNotification(ref WinProcMessage msg)
		{
			AppBarStates state;
			var msgType = (AppBarNotifications)(Int32)msg.WParam;
			switch (msgType)
			{
				case AppBarNotifications.PosChanged:
					//Trace.WriteLine("PosChanged");
					SizeAppBar(); 
					break;
				case AppBarNotifications.StateChange:
//					Trace.WriteLine("StateChange");
//					state = default(AppBarStates); //AppbarGetTaskbarState();
//					if ((state & AppBarStates.AlwaysOnTop) !=0)
//					{
//						//Topmost = true;
//						// BringToFront();
//					}
//					else
//					{
//						//Topmost = false;
//						//SendToBack();
//					}
					break;
				case AppBarNotifications.FullScreenApp:
					//Trace.WriteLine("FullScreenApp");
					if ((int)msg.LParam !=0)
					{
						Dispatcher.BeginInvoke((Action) delegate { Topmost = false; });
					}
					else
					{
						Dispatcher.BeginInvoke((Action)delegate { Topmost = true; });
						//state = default(AppBarStates); // AppbarGetTaskbarState();
//						if ((state & AppBarStates.AlwaysOnTop) !=0)
//						{
//							//Topmost = true;
//							//BringToFront();
//						}
//						else
//						{
//							//Topmost = false;
//							//SendToBack();
//						}
					}
					break;
				case AppBarNotifications.WindowArrange:
					Trace.WriteLine("WindowArrange");
					Visibility = (int)msg.LParam == 0 ? Visibility.Visible : Visibility.Hidden;
					break;
			}
		}

		[DllImport("user32")]
		private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

		bool _mute;

		void OnNcHitTest(ref WinProcMessage msg)
		{
			_mute = true;
			var res = SendMessage(msg.Hwnd, msg.Msg, msg.WParam, msg.LParam);
			_mute = false;
			// Trace.WriteLine(res.ToString());
			if ((_edge == AppBarEdges.Top) && ((int)msg.Result == (int)MousePositionCodes.HTBOTTOM))
			{
			}
			else if ((_edge == AppBarEdges.Bottom) && ((int)msg.Result == (int)MousePositionCodes.HTTOP))
			{
			}
			else if ((_edge == AppBarEdges.Left) && ((int)msg.Result == (int)MousePositionCodes.HTRIGHT))
			{
			}
			else if ((_edge == AppBarEdges.Right) && ((int)msg.Result == (int)MousePositionCodes.HTLEFT))
			{
			}
			else if ((int)msg.Result == (int)MousePositionCodes.HTCLOSE)
			{
			}
			else
			{
				msg.Result = (IntPtr)MousePositionCodes.HTCLIENT;
				msg.Handled = true;
			}
		}


		#endregion WinProcMessage Handlers

		#region Window Procedure

		struct WinProcMessage
		{
			public readonly IntPtr Hwnd;
			public readonly int Msg;
			public readonly IntPtr WParam;
			public readonly IntPtr LParam;
			public bool Handled;

			public IntPtr Result;

			public WinProcMessage(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam)
			{
				Hwnd = hwnd;
				Msg = msg;
				WParam = wParam;
				LParam = lParam;
				Handled = false;
				Result = IntPtr.Zero;
			}
		}

		protected IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
		{
			if (_mute)
			{
				handled = false;
				return IntPtr.Zero;
			}
			var m = new WinProcMessage(hwnd, msg, wParam, lParam);

			if (_isAppbarMode)
			{
				if (m.Msg == _callbackMessageId)
				{
					OnAppbarNotification(ref m);
				}
				else if (m.Msg == (int)WM.ACTIVATE)
				{
					AppbarActivate();
				}
				else if (m.Msg == (int)WM.WINDOWPOSCHANGED)
				{
					AppbarWindowPosChanged();
				}
				else if (m.Msg == (int)WM.NCHITTEST)
				{
					OnNcHitTest(ref m);
				}
			}

			handled = m.Handled;
			return m.Result;
		}
		
		#endregion Window Procedure

		protected override void OnClosing(CancelEventArgs e)
		{
			if (_edge != AppBarEdges.Float)
			{
				AppbarRemove();
			}
			base.OnClosing(e);
		}

		Size Size
		{
			get { return new Size(Width, Height); }
			set
			{
				if(Width != value.Width)
				{
					Width = value.Width;
				}
				if (Height != value.Height)
				{
					Height = value.Height;
				}
			}
		}

		Point Location
		{
			get { return new Point(Left, Top); }
			set
			{
				if(Left != value.X)
				{
					Left = value.X;
				}
				if (Top != value.Y)
				{
					Top = value.Y;
				}
			}
		}

		void ApplicationDesktopToolbarSizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (_isAppbarMode)
			{
				bool isModified = false;
				if (_edge == AppBarEdges.Top || _edge == AppBarEdges.Bottom)
				{
					isModified |= Math.Abs(_prevSize.Height - Size.Height) > 0.001;
					_prevSize.Height = Size.Height - HeightExtra;
				}
				else
				{
					isModified |= Math.Abs(_prevSize.Height - Size.Height) > 0.001;
					_prevSize.Width = Size.Width - WidthExtra;
				}
				if (isModified)
				{
					SizeAppBar();
				}
			}
		}

		public AppBarEdges Edge 
		{
			get 
			{
				return _edge;
			}
			set 
			{
				if (value != _edge)
				{
					_edge = value;
					if (value == AppBarEdges.Float)
					{
						AppbarRemove();
					}
					else
					{
						AppbarNew();
					}
					if (_isAppbarMode)
					{
						SizeAppBar();
					}
				}
			}
		}

		public double WidthExtra { get; set; }
		public double HeightExtra { get; set; }
	}
}