﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;

using MyUtils;

namespace XPlay
{
	public static class ThombstoneObservableCollectionWrapper
	{
		public static ThombstoneObservableCollectionWrapper<T> ThombstoneObservable<T>(this IEnumerable<T> collection)
		{
			return new ThombstoneObservableCollectionWrapper<T>(collection);
		}
	}

	public class Slot : ObservableObject
	{
		public Slot()
		{
			Created = DateTime.UtcNow;
		}

		public DateTime Created { get; private set; }

		bool _isTombstoned;
		public bool IsTombstoned
		{
			get { return _isTombstoned; }
			set
			{
				if (_isTombstoned != value)
				{
					_isTombstoned = value;
					OnPropertyChanged("IsTombstoned", !value, value);
				}
			}
		}
	}

	public class Slot<T> : Slot
	{
		public Slot(T value)
		{
//			ThreadPool.QueueUserWorkItem(delegate {
//				Thread.Sleep(100);
//				DefaultDispatcherInvoke(delegate {
//					IsTombstoned = false;
//				});
//			});
			Value = value;
		}

		public T Value { get; private set; }

		//		public static implicit operator T(Slot<T> slot)
		//		{
		//			return slot.Value;
		//		}
		//
		//		public static implicit operator Slot<T>(T item)
		//		{
		//			return new Slot<T>(item);
		//		}
		
		public override string ToString()
		{
			if (IsTombstoned)
			{
				return Value + " *";
			}
			return Value.ToString();
		}
	}

	public class ThombstoneObservableCollectionWrapper<T> : System.Collections.ObjectModel.ObservableCollection<Slot<T>>
	{
		// this instance is an output collection

		// this fields is an input collection
		readonly IEnumerable<T> _col;
		readonly INotifyCollectionChanged _colNcc;

		// sorted list of indexes for pending remove
		readonly HashSet<Slot<T>> _thombstones = new HashSet<Slot<T>>();
		// readonly List<T> _real = new List<T>();
		readonly List<Slot<T>> _realSlots = new List<Slot<T>>();

		readonly object _syncOperation = new object();

		public ThombstoneObservableCollectionWrapper(IEnumerable<T> col)
		{
			_col = col;
			_colNcc = col as INotifyCollectionChanged;
			if (_colNcc == null)
			{
				throw new ArgumentException("Collection is not observable");
			}
			lock (_syncOperation)
			{
				_colNcc.CollectionChanged += ColNccCollectionChanged;
				Reset();
			}

			_purge = new DelayedExecutor(PurgeWorker, 5000, true);
		}

		readonly DelayedExecutor _purge;

		void PurgeWorker()
		{
			DispatchableObservable.DefaultDispatcherInvoke(delegate {
				lock (_syncOperation)
				{
					using (new Monitor(this))
					{
						var initialCount = Count;
						var tombstones = _thombstones.Count;
						foreach (var thombstone in _thombstones)
						{
							Remove(thombstone);
						}
						_thombstones.Clear();
						Trace.WriteLine("{0} tombstones removed. {1} => {2}. Real Count {3}. Source Count {4}.".Arg(tombstones, initialCount, Count, _realSlots.Count, _col.Count()));
					}
				}
			});
		}

		int GetOutputIndex(int inputIndex)
		{
			if (inputIndex == 0)
			{
				return 0;
			}
			var after = _realSlots[inputIndex-1];
			var tombstones = this.TakeWhile(x => !Equals(x, after)).Count(x => _thombstones.Contains(x));
			return inputIndex + tombstones;
		}

//		int GetIndexPos(int x)
//		{
//		    var i = _pendingRemoveIndexes.BinarySearch(x);
//		    if (i < 0)
//		    {
//		        i = ~i;
//		    }
//		    return i;
//		}

		void ColNccCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			// supported only:
			// a) add 1 element
			// b) remove 1 element
			// c) reset
			lock (_syncOperation)
			{
				var line = "{0} {1} {2}".Arg(e.Action, e.OldStartingIndex, e.NewStartingIndex);
				Trace.WriteLine(line);

				switch (e.Action)
				{
					case NotifyCollectionChangedAction.Add:
						HandleAdd(e);
						break;
					case NotifyCollectionChangedAction.Remove:
						HandleRemove(e);
						break;
					case NotifyCollectionChangedAction.Replace:
					case NotifyCollectionChangedAction.Move:
						if (e.OldStartingIndex != e.NewStartingIndex || e.NewItems[0] != e.OldItems[0])
						{
							HandleRemove(e);
							HandleAdd(e);
						}
						break;
					case NotifyCollectionChangedAction.Reset:
						Reset();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		void HandleAdd(NotifyCollectionChangedEventArgs e)
		{
			Debug.Assert(e.NewItems != null);
			Debug.Assert(e.NewItems.Count == 1);
			var item = (T)e.NewItems[0];

			var slot = new Slot<T>(item);

//			if (!_thombstones.Contains(item))
//			{
				using (new Monitor(this))
				{
					Insert(GetOutputIndex(e.NewStartingIndex), slot);
				}
//			}
//			else
//			{
//				using (new Monitor(this))
//				{
//					// var slot2 = this.FirstOrDefault(x => Equals(x.Value, item));
//					Remove(slot2);
//					Insert(GetOutputIndex(e.NewStartingIndex), slot2);
//					slot2.IsTombstoned = false;
//					OnResurrected(new EventArgs<T>(item));
//				}
//			}

			_realSlots.Insert(e.NewStartingIndex, slot);
		}

		void HandleRemove(NotifyCollectionChangedEventArgs e)
		{
			Debug.Assert(e.OldItems != null);
			Debug.Assert(e.OldItems.Count == 1);
			var oldItem = (T)e.OldItems[0];

			var slots = this.Where(x => Equals(x.Value, oldItem));

			int c = 0;

			foreach (var slot in slots.OrderByDescending(x=>x.Created))
			{
				c++;
				if (c > 1)
				{
					using (new Monitor(this))
					{
						_thombstones.Remove(slot);
						Remove(slot);
					}
				}
				else
				{
					_thombstones.Add(slot);
					slot.IsTombstoned = true;
				}
			}

			_realSlots.RemoveAt(e.OldStartingIndex);

			OnRemoved(new EventArgs<T>(oldItem));

			_purge.Execute();
		}

		public event EventHandler<EventArgs<T>> Removed;
		public event EventHandler<EventArgs<T>> Resurrected;

		public void OnResurrected(EventArgs<T> e)
		{
			var handler = Resurrected;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected void OnRemoved(EventArgs<T> e)
		{
			var handler = Removed;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		void Reset()
		{
			// todo make incremental resync with pending remove feature
			using (new Monitor(this))
			{
				Clear();
				_realSlots.Clear();
				_thombstones.Clear();
				foreach (var item in _col)
				{
					var slot = new Slot<T>(item);
					Add(slot);
					_realSlots.Add(slot);
				}
			}
		}

		bool _isSourceEvent;

		class Monitor : IDisposable
		{
			readonly ThombstoneObservableCollectionWrapper<T> _col;

			readonly bool _old;

			public Monitor(ThombstoneObservableCollectionWrapper<T> col)
			{
				_col = col;
				_old = _col._isSourceEvent;
				_col._isSourceEvent = true;
			}

			public void Dispose()
			{
				_col._isSourceEvent = _old;
			}
		}

		protected override void ClearItems()
		{
			if(!_isSourceEvent)
			{
				throw new NotSupportedException();
			}
			base.ClearItems();
		}

		protected override void InsertItem(int index, Slot<T> item)
		{
			if(!_isSourceEvent)
			{
				throw new NotSupportedException();
			}
			base.InsertItem(index, item);
		}

		protected override void MoveItem(int oldIndex, int newIndex)
		{
			if (!_isSourceEvent)
			{
				throw new NotSupportedException();
			}
			base.MoveItem(oldIndex, newIndex);
		}

		protected override void RemoveItem(int index)
		{
			if (!_isSourceEvent)
			{
				throw new NotSupportedException();
			}
			base.RemoveItem(index);
		}

		protected override void SetItem(int index, Slot<T> item)
		{
			if (!_isSourceEvent)
			{
				throw new NotSupportedException();
			}
			base.SetItem(index, item);
		}
	}

	public class EventArgs<T> : EventArgs
	{
		readonly T _value;

		public T Value
		{
			get { return _value; }
		}

		public EventArgs(T value)
		{
			_value = value;
		}
	}
}
