﻿using System.ComponentModel;

using MetaCreator;

//namespace XPlay {
	namespace XPlay
	{
		public static class MetaExtension
		{
			public static void Pro(this IMetaWriter writer, string type, string name, object defaultValue = null)
			{
				var fieldName = "_" + char.ToLowerInvariant(name[0]) + name.Substring(1);
				writer.WriteLine(
					@"{0} {1};
[{3}({4})]
public {0} {2}
{{
	get
	{{
		return {1};
	}}
	set
	{{
		if ({1} != value)
		{{
			var old = {1};
			{1} = value;
			OnPropertyChanged(""{2}"", old, value);
		}}
	}}
}}
", type, fieldName, name, typeof(DefaultValueAttribute).CSharpTypeIdentifier(), defaultValue == null ? "null" : defaultValue.ToString());
			}
		}
	}
//}
