using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;

namespace XPlay
{
	public class DispatchableObservable : INotifyPropertyChanged, INotifyPropertyChanging
	{
		#region Dispatchable and ctor

		static Dispatcher DefaultDispatcher
		{
			get
			{
				var app = Application.Current;
				if (app != null)
				{
					return app.Dispatcher;
				}
				return null;
			}
		}

		public static void DefaultDispatcherInvoke(Action<object> act, object state = null)
		{
			if (act != null)
			{
				var disp = DefaultDispatcher;
				if (disp != null)
				{
					disp.BeginInvoke(act, state);
				}
			}
		}

		private readonly Dispatcher _dispatcher;

		public DispatchableObservable()
		{
			_dispatcher = DefaultDispatcher;
		}

		public DispatchableObservable(Dispatcher dispatcher)
		{
			_dispatcher = dispatcher;
		}

		#endregion

		public event PropertyChangedEventHandler PropertyChanged;
		public event PropertyChangingEventHandler PropertyChanging;
		public event EventHandler<PropertyChangeEventArgs> PropertyChange;

		public void OnPropertyInvalidate(string name = null)
		{
			var disp = _dispatcher;
			if (disp != null && disp != Dispatcher.CurrentDispatcher)
			{
				if (PropertyChanged != null)
				{
					disp.BeginInvoke((Action<string>)OnPropertyInvalidateInvoke, name);
				}
			}
			else
			{
				OnPropertyInvalidateInvoke(name);
			}
		}

		public virtual void OnPropertyChanged(string name, object oldValue, object newValue)
		{
			if (!Equals(oldValue, newValue))
			{
				var disp = _dispatcher;
				if (disp != null && disp != Dispatcher.CurrentDispatcher)
				{
					if (PropertyChanging != null || PropertyChanged != null || PropertyChange != null)
					{
						disp.BeginInvoke((Action<string, object, object>)OnPropertyChangeInvoke, name, oldValue, newValue);
					}
				}
				else
				{
					OnPropertyChangeInvoke(name, oldValue, newValue);
				}
			}
		}

		protected virtual void OnPropertyChangeInvoke(string name, object oldValue, object newValue)
		{
			var handlerPcIng = PropertyChanging;
			var handlerPc = PropertyChange;
			var handlerPcEd = PropertyChanged;

			if (handlerPcIng != null)
			{
				handlerPcIng(this, new PropertyChangingEventArgs(name));
			}
			if (handlerPc != null || handlerPcEd != null)
			{
				var pc = new PropertyChangeEventArgs(name, oldValue, newValue);
				if (handlerPc != null)
				{
					handlerPc(this, pc);
				}
				if (handlerPcEd != null)
				{
					handlerPcEd(this, pc);
				}
			}
		}

		void OnPropertyInvalidateInvoke(string name)
		{
			var handlerPcEd = PropertyChanged;

			if (handlerPcEd != null)
			{
				handlerPcEd(this, new PropertyChangedEventArgs(name));
			}
		}
	}
}