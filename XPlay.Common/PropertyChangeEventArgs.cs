using System.ComponentModel;

namespace XPlay
{
	public class PropertyChangeEventArgs : PropertyChangedEventArgs
	{
		readonly object _oldValue;
		public object OldValue
		{
			get { return _oldValue; }
		}

		readonly object _newValue;
		public object NewValue
		{
			get { return _newValue; }
		}

		public PropertyChangeEventArgs(string name, object oldValue, object newValue):base(name)
		{
			_oldValue = oldValue;
			_newValue = newValue;
		}
	}
}