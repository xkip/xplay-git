namespace XPlay
{
	public class ObservableObject : DispatchableObservable
	{
		public void Save()
		{
			OnPropertyInvalidate("_save_");
		}
	}
}