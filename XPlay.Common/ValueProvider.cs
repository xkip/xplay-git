using System;

using Obtics.Values;

namespace XPlay
{
	class ValueProvider<T> : IValueProvider<T>
	{
		readonly Func<T> _func;
		readonly Action<T> _set;

		public ValueProvider(Func<T> func, Action<T> set)
		{
			_func = func;
			_set = set;
		}

		object IValueProvider.Value
		{
			get { return Value; }
			set { Value = (T)value; }
		}

		public T Value
		{
			get { return _func(); }
			set { _set(value); }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}
	}
}