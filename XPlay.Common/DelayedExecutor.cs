﻿using System;
using System.Timers;
using System.Xaml;

namespace XPlay
{
	public static class AttachablePropertyServicesExt
	{
		public static T GetAttached<T, TD>(this object item, string name)
		{
			T value;
			if (AttachablePropertyServices.TryGetProperty(item, new AttachableMemberIdentifier(typeof(TD), name), out value))
			{
				return value;
			}
			return default(T);
		}

		public static void SetAttached<T, TD>(this object item, string name, T value)
		{
			AttachablePropertyServices.SetProperty(item, new AttachableMemberIdentifier(typeof (TD), name), value);
		}
	}

	public class DelayedExecutor
	{
		private readonly Action _act;
		private readonly Timer _timer;
		private readonly bool _onlyOnStopSending;

		public DelayedExecutor(Action act, int interval = 100, bool onlyOnStopSending = false)
		{
			if (act == null) throw new ArgumentNullException("act");
			_act = act;
			_onlyOnStopSending = onlyOnStopSending;
			_timer = new Timer(interval);
			_timer.Elapsed += TimerElapsed;
		}

		void TimerElapsed(object sender, ElapsedEventArgs e)
		{
			_timer.Enabled = false;
			_act();
		}



		public void Execute()
		{
			if (_onlyOnStopSending || _timer.Enabled == false)
			{
				_timer.Enabled = false;
				_timer.Enabled = true;
			}
		}
	}
}
