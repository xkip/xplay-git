sampler2D  Sampler : register(S0);

float4 main(float2 uv : TEXCOORD) : COLOR
{
	float4 px = tex2D(Sampler, uv);

	float4 c = 0;
	for (int i = 0; i < 9; i++)
	{
		c += tex2D(Sampler, uv + float2( (i - 3) * 0.04, 0));
	}
	c /= 9;

	c.a = 1;

	return c;
}
