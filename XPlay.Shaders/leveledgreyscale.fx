float Level : register(C0);
sampler2D  ImageSampler : register(S0);

float4 main( float2 uv : TEXCOORD) : COLOR
{
	float3	lumw = float3(0.299,0.587,0.114);
	float4	pixel = tex2D(ImageSampler, uv);
	float	lum = dot(pixel.rgb,lumw);
	pixel.rgb = lerp(lum,pixel.rgb,Level);
	return pixel;

//	Simple:
//	float4 color = tex2D( ImageSampler, uv);
//	/// float gr = (color.r + color.g + color.b ) / 3;
//	float gr = color.r * 0.3 + color.g * 0.59 + color.b * 0.11;
//	color.r = color.r * (1-Level) + gr * Level;
//	color.g = color.g * (1-Level) + gr * Level;
//	color.b = color.b * (1-Level) + gr * Level;
//	return color;
}
