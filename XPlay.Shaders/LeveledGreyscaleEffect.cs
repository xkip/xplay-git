using System.Windows;
using System.Windows.Media;

using XPlay.Shaders.Internal;

namespace XPlay.Shaders
{
	[Shader("leveledgreyscale.ps")]
	public class LeveledGreyscaleEffect : CustomEffect<LeveledGreyscaleEffect>
	{
		public LeveledGreyscaleEffect()
		{
			UpdateShaderValue(LevelProperty);
		}

		public static readonly DependencyProperty LevelProperty = DependencyProperty.Register("Level", typeof(float), typeof(LeveledGreyscaleEffect), new UIPropertyMetadata(0f, PixelShaderConstantCallback(0)));

		/// <summary>
		/// Gets or sets Input properties. 
		/// </summary>
		public float Level
		{
			get { return (float)GetValue(LevelProperty); }
			set { SetValue(LevelProperty, value); }
		}

	}
}