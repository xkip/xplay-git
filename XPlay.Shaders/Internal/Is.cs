using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace XPlay.Shaders.Internal
{
	static class Is
	{
		static readonly UserControl _test = new UserControl();

		public static bool DesignMode
		{
			get { return _test.IsDesignMode(); }
		}

		public static bool IsDesignMode(this DependencyObject control)
		{
			return DesignerProperties.GetIsInDesignMode(_test);
		}
	}
}