using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;

using MyUtils;

namespace XPlay.Shaders.Internal
{
	// ReSharper disable StaticFieldInGenericType
	public class CustomEffect<T> : ShaderEffect
	{
		/// <summary>
		/// Dependency property for Input.
		/// </summary>
		public static readonly DependencyProperty InputProperty = RegisterPixelShaderSamplerProperty("Input", typeof(T), 0 /* assigned to sampler register S0 */);

		/// <summary>
		/// PixelShader for this effect.
		/// </summary>
		private static readonly Lazy<PixelShader> _pixelShader;

		/// <summary>
		/// Static constructor - Create a PixelShader for all GreyscaleEffect instances. 
		/// </summary>
		static CustomEffect()
		{
			_pixelShader = new Lazy<PixelShader>(() => {
				var ps = new PixelShader {UriSource = new Uri("pack://application:,,,/XPlay.Shaders;component/" + typeof(T).Attribute<ShaderAttribute>().Shader)};
				ps.Freeze();
				return ps;
			});
		}

		/// <summary>
		/// Constructor - Assign the PixelShader property and set the shader parameters to default values.
		/// </summary>
		public CustomEffect()
		{
			if (!this.IsDesignMode())
			{
				PixelShader = _pixelShader.Value;
				UpdateShaderValue(InputProperty);
			}
		}

		/// <summary>
		/// Gets or sets Input properties. 
		/// </summary>
		public Brush Input
		{
			get { return (Brush)GetValue(InputProperty); }
			set { SetValue(InputProperty, value); }
		}
	}
}