using System;

namespace XPlay.Shaders.Internal
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
	sealed class ShaderAttribute : Attribute
	{
		readonly string _shader;

		public ShaderAttribute(string shader)
		{
			_shader = shader;
		}

		public string Shader
		{
			get { return _shader; }
		}
	}
}