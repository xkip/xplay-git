﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Markup;
using XPlay;

namespace XPlay.Model
{
//	public static class NegotiationExt
//	{
//		public static NegotiationSettings GetNegotiation(this object item)
//		{
//			var neg = item.GetAttached<NegotiationSettings, Negotiation>("NegotiationSettings");
//			if (neg == null)
//			{
//				neg = new NegotiationSettings();
//				SetNegotiation(item, neg);
//			}
//			return neg;
//		}
//
//		public static void SetNegotiation(this object item, NegotiationSettings value)
//		{
//			item.SetAttached<NegotiationSettings, Negotiation>("NegotiationSettings", value);
//		}
//
//	}
//
//	public class Negotiation
//	{
//		public static DependencyProperty NegotiationSettingsProperty = DependencyProperty.RegisterAttached("NegotiationSettings", typeof(NegotiationSettings), typeof(Negotiation));
//
//		public static NegotiationSettings GetNegotiationSettings(DependencyObject item)
//		{
//			return (NegotiationSettings)item.GetValue(NegotiationSettingsProperty);
//		}
//
//		public static void SetNegotiationSettings(DependencyObject item, NegotiationSettings value)
//		{
//			item.SetValue(NegotiationSettingsProperty, value);
//		}
//
//	}

//	[ContentProperty("NegotiationProperties")]
//	public class NegotiationSettings
//	{
//		readonly IList<NegotiationProperty> _negotiationProperties = new List<NegotiationProperty>();
//
//		public IList<NegotiationProperty> NegotiationProperties
//		{
//			get { return _negotiationProperties; }
//		}
//	}

	[ContentProperty("Negotiators")]
	public class NegotiationProperty : ObservableObject, IEquatable<NegotiationProperty>
	{
		public NegotiationProperty()
		{
			
		}

		public NegotiationProperty(string name)
		{
			Name = name;
		}

		readonly IList<NegotiatorBase> _negotiators = new ObservableCollection<NegotiatorBase>();

		public IList<NegotiatorBase> Negotiators
		{
			get { return _negotiators; }
		}

		public string Name { get; set; }

		/*# Pro("DateTime", "NegotiationPhase") */
		/*# Pro("bool", "Active") */

		#region Equals
		public bool Equals(NegotiationProperty other)
		{
			if (ReferenceEquals(null, other))
			{
				return false;
			}
			if (ReferenceEquals(this, other))
			{
				return true;
			}
			return Equals(other.Name, Name);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}
			if (ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != typeof(NegotiationProperty))
			{
				return false;
			}
			return Equals((NegotiationProperty)obj);
		}

		public override int GetHashCode()
		{
			return (Name != null ? Name.GetHashCode() : 0);
		} 
		#endregion
	}

//	public class NegotiatorAuthor : NegotiatorBase
//	{
//
//	}
}
