using System;

/*@ fileInProject */
/*@ using XPlay */

namespace XPlay.Model
{
	public abstract class NegotiatorBase : ObservableObject, IEquatable<NegotiatorBase>
	{
		public NegotiatorBase()
		{
			
		}

		public NegotiatorBase(Guid id)
		{
			Id = id;
		}

		public Guid Id { get; set; }

		/*# Pro("object", "Vote") */
		/*# Pro("DateTime", "Date") */

		#region Equals
		public bool Equals(NegotiatorBase other)
		{
			if (ReferenceEquals(null, other))
			{
				return false;
			}
			if (ReferenceEquals(this, other))
			{
				return true;
			}
			return other.Id.Equals(Id);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}
			if (ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != typeof(NegotiatorBase))
			{
				return false;
			}
			return Equals((NegotiatorBase)obj);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		} 
		#endregion
	}
}