﻿
#line 1 "C:\Users\x.D\Desktop\Dropbox\XPlay\XPlay.Model\XTask_partial.cs"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XPlay.Model
{
	partial class XTask
	{
		
#line default


		

		string _title;
[System.ComponentModel.DefaultValueAttribute(null)]
public string Title
{
	get
	{
		return _title;
	}
	set
	{
		if (_title != value)
		{
			var old = _title;
			_title = value;
			OnPropertyChanged("Title", old, value);
		}
	}
}


		DateTime? _startMin;
[System.ComponentModel.DefaultValueAttribute(null)]
public DateTime? StartMin
{
	get
	{
		return _startMin;
	}
	set
	{
		if (_startMin != value)
		{
			var old = _startMin;
			_startMin = value;
			OnPropertyChanged("StartMin", old, value);
		}
	}
}


		DateTime? _endMax;
[System.ComponentModel.DefaultValueAttribute(null)]
public DateTime? EndMax
{
	get
	{
		return _endMax;
	}
	set
	{
		if (_endMax != value)
		{
			var old = _endMax;
			_endMax = value;
			OnPropertyChanged("EndMax", old, value);
		}
	}
}


		DateTime? _completedAt;
[System.ComponentModel.DefaultValueAttribute(null)]
public DateTime? CompletedAt
{
	get
	{
		return _completedAt;
	}
	set
	{
		if (_completedAt != value)
		{
			var old = _completedAt;
			_completedAt = value;
			OnPropertyChanged("CompletedAt", old, value);
		}
	}
}


		string _iconName;
[System.ComponentModel.DefaultValueAttribute(null)]
public string IconName
{
	get
	{
		return _iconName;
	}
	set
	{
		if (_iconName != value)
		{
			var old = _iconName;
			_iconName = value;
			OnPropertyChanged("IconName", old, value);
		}
	}
}


		Guid? _assignedTo;
[System.ComponentModel.DefaultValueAttribute(null)]
public Guid? AssignedTo
{
	get
	{
		return _assignedTo;
	}
	set
	{
		if (_assignedTo != value)
		{
			var old = _assignedTo;
			_assignedTo = value;
			OnPropertyChanged("AssignedTo", old, value);
		}
	}
}


		string _comment;
[System.ComponentModel.DefaultValueAttribute(null)]
public string Comment
{
	get
	{
		return _comment;
	}
	set
	{
		if (_comment != value)
		{
			var old = _comment;
			_comment = value;
			OnPropertyChanged("Comment", old, value);
		}
	}
}


		DateTime? _createdAt;
[System.ComponentModel.DefaultValueAttribute(null)]
public DateTime? CreatedAt
{
	get
	{
		return _createdAt;
	}
	set
	{
		if (_createdAt != value)
		{
			var old = _createdAt;
			_createdAt = value;
			OnPropertyChanged("CreatedAt", old, value);
		}
	}
}


		Guid? _createdBy;
[System.ComponentModel.DefaultValueAttribute(null)]
public Guid? CreatedBy
{
	get
	{
		return _createdBy;
	}
	set
	{
		if (_createdBy != value)
		{
			var old = _createdBy;
			_createdBy = value;
			OnPropertyChanged("CreatedBy", old, value);
		}
	}
}


#line 22 "C:\Users\x.D\Desktop\Dropbox\XPlay\XPlay.Model\XTask_partial.cs"

	}
}

#line default
