using System;
using XPlay;

namespace XPlay.Model
{
	public class User : ObservableObject, IEquatable<User>
	{
		public Guid Id { get; set; }

		/*string _userName;
		public string UserName
		{
			get { return _userName; }
			set
			{
				if (_userName != value)
				{
					var old = _userName;
					_userName = value;
					OnPropertyChanged("UserName", old, value);
				}
			}
		}*/

		/*# Pro("string", "UserName")*/

		public bool Equals(User other)
		{
			if (ReferenceEquals(null, other))
			{
				return false;
			}
			if (ReferenceEquals(this, other))
			{
				return true;
			}
			return other.Id.Equals(Id);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}
			if (ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != typeof (User))
			{
				return false;
			}
			return Equals((User)obj);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		public override string ToString()
		{
			return UserName;
		}
	}
}