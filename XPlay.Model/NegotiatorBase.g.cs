﻿
#line 1 "C:\Users\x.D\Desktop\Dropbox\XPlay\XPlay.Model\NegotiatorBase.cs"
using System;


#line default



namespace XPlay.Model
{
	public abstract class NegotiatorBase : ObservableObject, IEquatable<NegotiatorBase>
	{
		public NegotiatorBase()
		{
			
		}

		public NegotiatorBase(Guid id)
		{
			Id = id;
		}

		public Guid Id { get; set; }

		object _vote;
[System.ComponentModel.DefaultValueAttribute(null)]
public object Vote
{
	get
	{
		return _vote;
	}
	set
	{
		if (_vote != value)
		{
			var old = _vote;
			_vote = value;
			OnPropertyChanged("Vote", old, value);
		}
	}
}


		DateTime _date;
[System.ComponentModel.DefaultValueAttribute(null)]
public DateTime Date
{
	get
	{
		return _date;
	}
	set
	{
		if (_date != value)
		{
			var old = _date;
			_date = value;
			OnPropertyChanged("Date", old, value);
		}
	}
}


#line 23 "C:\Users\x.D\Desktop\Dropbox\XPlay\XPlay.Model\NegotiatorBase.cs"


		#region Equals
		public bool Equals(NegotiatorBase other)
		{
			if (ReferenceEquals(null, other))
			{
				return false;
			}
			if (ReferenceEquals(this, other))
			{
				return true;
			}
			return other.Id.Equals(Id);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}
			if (ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != typeof(NegotiatorBase))
			{
				return false;
			}
			return Equals((NegotiatorBase)obj);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		} 
		#endregion
	}
}
#line default
