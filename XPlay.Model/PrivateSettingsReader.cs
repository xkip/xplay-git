using System.IO;
using System.Xaml;

namespace XPlay.Model
{
	public class PrivateSettingsReader
	{
		private readonly string _settingsPath;

		public PrivateSettingsReader(string settingsPath)
		{
			_settingsPath = settingsPath;
		}

		private PrivateSettings _settings;
		public PrivateSettings Settings
		{
			get
			{
				if (_settings == null)
				{
					var file = FileName;
					if (File.Exists(file))
					{
						_settings = (PrivateSettings)XamlServices.Load(file);
					}
					else
					{
						_settings = new PrivateSettings();
					}
				}
				return _settings;
			}
		}

		private string FileName
		{
			get
			{
				var file = Path.Combine(_settingsPath, "PrivateSettings.xaml");
				return file;
			}
		}

		public void Save()
		{
			XamlServices.Save(FileName, Settings);
		}
	}
}