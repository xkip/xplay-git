﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Markup;
using XPlay;

using MyUtils;

[assembly: XmlnsDefinition("XPlay", "XPlay.Model")]

namespace XPlay.Model
{
	public partial class XTask : Negotiatable
	{
		public static XTask CreaateNew(User createdBy)
		{
			var at = DateTime.UtcNow;
			var task = new XTask
			{
				Title = "New Task",
				CreatedAt = at,
				CreatedBy = createdBy.Id,
				Comment = "Created at {0} by {1}".Arg(at, createdBy),
			};
			task.EnableChangeTracking();
			return task;
		}


		private int _priority = 500;

		[DefaultValue(500)]
		public int Priority
		{
			get { return _priority; }
			set
			{
				if (value > 999)
				{
					value = 999;
				}
				if (value < 1)
				{
					value = 1;
				}
				if (_priority != value)
				{
					var old = _priority;
					_priority = value;
					OnPropertyChanged("Priority", old, value);
					OnPropertyInvalidate("Abc");
				}
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public char Abc
		{
			get
			{
				if (_priority >= 800)
				{
					return 'A';
				}
				if (_priority >= 600)
				{
					return 'B';
				}
				return 'C';
			}
			set
			{
				if (Abc != value)
				{
					switch (value)
					{
						case 'A':
							Priority = 900; // middle of last 2/5 of higher half (500..1000)
							break;
						case 'B':
							Priority = 700; // middle of mid 2/5 of higher half (500..1000)
							break;
						case 'C':
							Priority = 500;
							break;
					}
				}
			}
		}

		private bool _isDone;

		[DefaultValue(false)]
		public bool IsDone
		{
			get { return _isDone; }
			set
			{
				if (_isDone != value)
				{
					var old = _isDone;
					_isDone = value;
					OnPropertyChanged("IsDone", old, value);
					if (value)
					{
						CompletedAt = DateTime.UtcNow;
					}
				}
			}
		}


		string _negotiatedComment;

		[DefaultValue(null)]
		public string NegotiatedComment
		{
			get { return _negotiatedComment; }
			set
			{
				if (_negotiatedComment != value)
				{
					var old = _negotiatedComment;
					_negotiatedComment = value;
					OnPropertyChanged("NegotiatedComment", old, value);
					OnPropertyInvalidate("NegotiatedCommentN");
				}
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string NegotiatedCommentN
		{
			get { return (string)GetNegotiatableValue("NegotiatedComment"); }
			set
			{
				SetNegotiatableValue("NegotiatedComment", value);
				OnPropertyInvalidate("NegotiatedCommentN");
			}
		}

		public override string ToString()
		{
			return Title;
		}

	}

	public abstract class HistoryEvent
	{
		public Guid Author { get; set; }
		public DateTime Date { get; set; }
	}

	public class PropertyHistoryEvent : HistoryEvent
	{
		public string PropertyName { get; set; }
		public object OldValue { get; set; }
		public object NewValue { get; set; }
	}
}
