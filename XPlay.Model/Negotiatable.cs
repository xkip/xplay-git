using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Markup;

namespace XPlay.Model
{
	[ContentProperty("NegotiationProperties")]
	public abstract class Negotiatable : ObservableObject//, IDirtiable
	{
		readonly IList<NegotiationProperty> _negotiationProperties = new ObservableCollection<NegotiationProperty>();

		public IList<NegotiationProperty> NegotiationProperties
		{
			get { return _negotiationProperties; }
		}

		public static Guid Me { get; set; }

		public object GetNegotiatableValue(string property)
		{
			var pro = _negotiationProperties.FirstOrDefault(x => x.Name == property);
			if (pro != null && pro.Active)
			{
				var usr = pro.Negotiators.FirstOrDefault(x => x.Id == Me);
				if (usr != null && usr.Vote != null && usr.Date > pro.NegotiationPhase)
				{
					return usr.Vote;
				}
			}
			return GetType().GetProperty(property).GetValue(this, null);
		}

		public void SetNegotiatableValue(string property, object value)
		{
			var pro = _negotiationProperties.FirstOrDefault(x => x.Name == property);
			var pi = GetType().GetProperty(property);

			if (pro != null && pro.Negotiators.Any() && !(pro.Negotiators.Count == 1 && pro.Negotiators.Single().Id == Me))
			{
				var me = pro.Negotiators.FirstOrDefault(x => x.Id == Me);
				if (me == null)
				{
					me = new Negotiator() {Id = Me};
					pro.Negotiators.Add(me);
				}
				me.Date = DateTime.UtcNow;
				me.Vote = value;
				if (pro.Negotiators.All(x => x.Date > pro.NegotiationPhase) && pro.Negotiators.All(x => Equals(x.Vote , pro.Negotiators.First().Vote)))
				{
					pro.Active = false;
					pro.NegotiationPhase = DateTime.UtcNow;
					pi.SetValue(this, value, null);
				}
				else
				{
					pro.Active = true;
				}
			}
			else
			{
				pi.SetValue(this, value, null);
			}
			Save();
			OnPropertyInvalidate();
		}

		readonly ObservableCollection<HistoryEvent> _history = new ObservableCollection<HistoryEvent>();

		public IList<HistoryEvent> History
		{
			get { return _history; }
		}

		public override void OnPropertyChanged(string name, object oldValue, object newValue)
		{
			lock (GetChangeTrackingSync())
			{
				base.OnPropertyChanged(name, oldValue, newValue);
			}
		}

		protected override void OnPropertyChangeInvoke(string name, object oldValue, object newValue)
		{
			if (_enableChangeTracking)
			{
				var dtNow = DateTime.UtcNow;
				var last = _history.OfType<PropertyHistoryEvent>().LastOrDefault(x => x.Author == Me && x.PropertyName == name);
				if (last != null && dtNow - last.Date < TimeSpan.FromSeconds(10))
				{
					//var index = _history.IndexOf(last);
					last.Date = dtNow;
					last.NewValue = newValue;
					_history.Remove(last);
					_history.Add(last);
					//_history.Move(index, _history.Count);
				}
				else
				{
					_history.Add(new PropertyHistoryEvent
					{
						Author = Me,
						Date = dtNow,
						PropertyName = name,
						OldValue = oldValue,
						NewValue = newValue,
					});
				}
			}
			base.OnPropertyChangeInvoke(name, oldValue, newValue);
		}

		bool _enableChangeTracking;

		public void EnableChangeTracking(bool enable = true)
		{
			lock (_changeTrackingSync)
			{
				_enableChangeTracking = enable;
			}
		}

		readonly object _changeTrackingSync = new object();

		public object GetChangeTrackingSync()
		{
			return _changeTrackingSync;
		}
	}
}