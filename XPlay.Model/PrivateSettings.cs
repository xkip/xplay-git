﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XPlay.Model
{
	public class PrivateSettings
	{
		public PrivateSettings()
		{
			MyUserId = Guid.NewGuid();
			TransparencyMode = 1;
			DockMode = 1;
		}

		public Guid MyUserId { get; set; }
		public int TransparencyMode { get; set; }
		public int DockMode { get; set; }
	}
}
