﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using MyUtils;

using XPlay;

namespace TestProject1
{
	[TestClass]
	public class TombstoneObservableCollection
	{
		class Item
		{
			readonly string _value;

			public Item()
			{
				_value = 100.Random().ToString(CultureInfo.InvariantCulture);
			}

			public override string ToString()
			{
				return _value;
			}
		}

		public TombstoneObservableCollection()
		{
			_src = new ObservableCollection<Item> {new Item(), new Item(),};
			_sut = new ThombstoneObservableCollectionWrapper<Item>(_src);
			_sut.CollectionChanged += _sut_CollectionChanged;
		}

		void _sut_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			_events.Add(e);
		}

		ObservableCollection<Item> _src;
		ThombstoneObservableCollectionWrapper<Item> _sut;
		List<NotifyCollectionChangedEventArgs> _events = new List<NotifyCollectionChangedEventArgs>();

		[TestMethod]
		public void Should_init_with_existing_elements()
		{
			Assert.AreEqual(2, _sut.Count);
			Assert.AreEqual(_src[0], _sut[0].Value);
			Assert.AreEqual(_src[1], _sut[1].Value);
		}

		[TestMethod]
		public void Should_add_item()
		{
			var item = new Item();

			_src.Insert(1, item);

			Assert.AreEqual(3, _src.Count);
			Assert.AreEqual(item, _src[1]);

			Assert.AreEqual(3, _sut.Count);
			Assert.AreEqual(_src[0], _sut[0].Value);
			Assert.AreEqual(_src[1], _sut[1].Value);
			Assert.AreEqual(_src[2], _sut[2].Value);

			Assert.AreEqual(1, _events.Count);
			Assert.AreEqual(NotifyCollectionChangedAction.Add, _events[0].Action);
			Assert.AreEqual(1, _events[0].NewStartingIndex);
			Assert.AreEqual(item, ((Slot<Item>)(_events[0].NewItems[0])).Value);
		}

		[TestMethod]
		public void Should_clear_items()
		{
			_src.Clear();
			Assert.AreEqual(0, _sut.Count);
			Assert.AreEqual(1, _events.Count);
			Assert.AreEqual(NotifyCollectionChangedAction.Reset, _events[0].Action);
		}

		[TestMethod]
		public void Should_not_remove_items()
		{
			_src.Add(new Item()); // let we have 3 of them
			_events.Clear();

			_src.RemoveAt(1);
			Assert.AreEqual(3, _sut.Count);
			Assert.AreEqual(0, _events.Count);
			Assert.AreEqual(_src[0], _sut[0].Value);
			Assert.AreEqual(_src[1], _sut[2].Value);
		}

		[TestMethod]
		public void Should_maintain_indexes()
		{
			_src.Add(new Item()); // let we have 3 of them
			var removed = _src[1];
			_src.Remove(removed);
			_events.Clear();

			_src.Insert(0, new Item());
			_src.Insert(3, new Item());
			_src.Insert(3, new Item());
			_src.Insert(2, new Item());
			_src.Insert(1, new Item());

			var expected = _src.ToList();
			expected.Insert(4, removed); // since we already inserted before thumb

			Console.WriteLine("E: " + expected.Select(x => x.ToString()).Join(", "));
			Console.WriteLine("A: " + _sut.Select(x => x.ToString()).Join(", "));

			for (int i = 0; i < expected.Count; i++)
			{
				Assert.AreEqual(expected[i], _sut[i].Value);
			}
		}

		[TestMethod]
		public void Should_allow_reinser_removed()
		{
			var r = _src[1];
			_src.Remove(r);
			_src.Add(r);

			Assert.AreEqual(2, _src.Count);
			Assert.AreEqual(2, _sut.Count);
			Assert.AreEqual(0, _events.Count);
		}
	}
}
